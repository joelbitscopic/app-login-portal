﻿using com.bitscopic.praediauth.dao;
using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.bitscopic.praediauth
{
    public partial class SecretBackdoor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void backdoor_login(Object sender, EventArgs e)
        {

            try
            {
                String email = vaEmail.Text;

                if (String.IsNullOrEmpty(email))
                {
                    setMessage("You really couldn't fill out the one textbox??");
                    return;
                }


                try
                {
                    BitscopicUser dbUser = getUserByPIVEmail(email);
                    setSessionAndRedirect(dbUser, null);
                    return;
                }
                catch (Exception lookupExc)
                {
                    if (lookupExc.Message.ToLower().Contains("not found"))
                    {
                        setMessage("Email not found!");
                        return;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.Log("An error occurred during backdoor login: " + exc.Message);
                setMessage("Whoops... something went wrong. Please contact your prettiest Bitscopic support person (Joel...) for assistance: " + exc.Message);
            }

        }
        void setMessage(String message)
        {
            labelMessage.Text = message;
        }
        void setSessionAndRedirect(BitscopicUser user, String redirectTo)
        {
            LogUtils.Log("Successfully authenticated " + user.email + " through backdoor login!!");
            String jwt = new JWTUtils().getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
            String fullRedirectUrl = "https://support.bitscopic.com/jwtaccess?jwt=" + jwt + (String.IsNullOrEmpty(redirectTo) ? "&returnTo=https://support.bitscopic.com" : "&returnTo=" + redirectTo);
            LogUtils.Log("Sending user to: " + fullRedirectUrl);
            Response.Redirect(fullRedirectUrl, true);
        }

        BitscopicUser getUserByPIVEmail(String email)
        {
            using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
            {
                cxn.Open();
                SqliteUserDao userDao = new SqliteUserDao(cxn);
                return userDao.getUserByIdAndIdentityProvider(email, "PIV");
            }
        }
    }
}