﻿using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using System;
using System.Configuration;

namespace com.bitscopic.praediauth
{
    public partial class AuthService : System.Web.UI.Page
    {
        public static String _session = "MySession";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params == null || Request.Params.Count == 0)
            {
                labelMessage.Text = "Invalid request - missing URL args";
                return;
            }

            String clientIdVar = Request.Params.Get("client_id");
            if (String.IsNullOrEmpty(clientIdVar))
            {
                labelMessage.Text = "Invalid request - missing client_id";
                return;
            }

            if (null == getClient(clientIdVar))
            {
                labelMessage.Text = "Invalid request - client_id " + clientIdVar + " not registered";
                return;
            }

            //String authMethod = "IAM";
            //if (false == String.IsNullOrEmpty(Request.Params.Get("auth_method")))
            //{
            //    authMethod = Request.Params.Get("auth_method");
            //}

            String clientRedirectVar = Request.Params.Get("redirect_uri");
            if (String.IsNullOrEmpty(clientRedirectVar))
            {
                labelMessage.Text = "Invalid request - missing redirect_uri";
                return;
            }
            if (clientRedirectVar.Contains("?"))
            {
                labelMessage.Text = "Invalid redirect_uri - do not pass URL params to this auth service";
                return;
            }

            LogUtils.Log("Received valid auth service request: " + Request.RawUrl);
            //https://login.bitsopic.va.gov/authservice/authservice.aspx?state=myState&client_id=praedigene_dev&redirect_uri=https://praedigene.va.gov/authservice_login&sts=false

            String idpStateVar = getNewIDPState();


            String clientStateVar = Request.Params.Get("state");
            bool clientFetchSTSVar = true;
            if (String.IsNullOrEmpty(clientStateVar))
            {
                clientStateVar = idpStateVar;
            }
            if (false == String.IsNullOrEmpty(Request.Params.Get("sts")) && String.Equals("false", Request.Params["sts"], StringComparison.CurrentCultureIgnoreCase))
            {
                clientFetchSTSVar = false;
            }

            if (String.Equals("true", MyConfigurationManager.getValue("ALWAYS_FETCH_STS"), StringComparison.CurrentCultureIgnoreCase))
            {
                clientFetchSTSVar = true;
            }

            ClientState clientState = new ClientState(state: clientStateVar, redirectUrl: clientRedirectVar);
            clientState.clientId = clientIdVar;
            clientState.flags = new System.Collections.Generic.Dictionary<string, string>(); 
            clientState.flags.Add("sts", (clientFetchSTSVar ? "true" : "false"));

            // fOMB2pxvphpSp7AVVypl
            // Bitscopic_NP
            // https://vhapalapppg02.v21.med.va.gov/DEV_login_portal/iam_sso_login
            // https://int.fed.eauth.va.gov/oauthi/sps/oauth/oauth20/authorize?client_id=%3cclientId%3e&response_type=code&aud=rsrServer1%20rsrServer2&scope=openid%20fhirUser&state=1L&redirect_uri=%3cregistered_redirect_uri
            // <add key="IAM_URL" value="https://int.fed.eauth.va.gov/oauthi/sps/oauth/oauth20/authorize?client_id=Bitscopic_NP&amp;response_type=code&amp;scope=openid%20fhirUser&amp;state=1L&amp;redirect_uri="/>
	        // <add key="IAM_REDIRECT_URL" value="https://vhapalapppg02.v21.med.va.gov/DEV_login_portal/iam_sso_login"/>

            String iamURL = "https://ssoi.idev.iam.va.gov/va-single-sign-on?state=";
            String redirectURL = "&redirect_uri=https://vhapalapppg02.v21.med.va.gov/login_portal/iam_sso_login.aspx";
            //    String formattedURL = "client_id={0}&response_type=code&redirect_uri={1}&scope={2}&state={3}";
            //    String scopeStr = "openid%20fhirUser";

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IAM_URL"]))
            {
                iamURL = ConfigurationManager.AppSettings["IAM_URL"];
            }
            iamURL = String.Concat(iamURL, idpStateVar); // FIRST: add state var to URL

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IAM_REDIRECT_URL"]))
            {
                redirectURL = ConfigurationManager.AppSettings["IAM_REDIRECT_URL"]; // SECOND: add redirect URL
            }

            String clientStateCompositeKey = String.Concat(clientIdVar, "|", clientStateVar); // concat the client's API key with a pipe char and then client state var to serve as the client state key
            IDPState idpState = new IDPState(state: idpStateVar, redirectUrl: redirectURL);
            idpState.client_state = clientStateCompositeKey; // map the identity provider state to the requesting client state for easy retrieval later

            MyConfigurationManager.setValue(idpStateVar, SerializerUtils.serialize(idpState, false)); // just saving these to MyConfigurationManager for now as a simple in-memory cache
            MyConfigurationManager.setValue(clientStateCompositeKey, SerializerUtils.serialize(clientState, false));  // just saving these to MyConfigurationManager for now as a simple in-memory cache 

            try
            {
                LogUtils.Log("Redirecting to: " + iamURL + "&redirect_uri=" + HttpUtils.encodeUrl(redirectURL));
                Response.Redirect(iamURL + "&redirect_uri=" + HttpUtils.encodeUrl(redirectURL), true);
                return;
            }
            catch (Exception)
            {
                //swallow thread abort
            }
        }

        private AuthServiceClient getClient(string clientId)
        {
            if (String.IsNullOrEmpty(MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId)))
            {
                return null;
            }
            return new AuthServiceClient() { clientId = clientId, sharedSecret = MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId) };
        }

        private static int _idpStateVar = 1;
        private string getNewIDPState()
        {
            return "" + _idpStateVar++.ToString() + "L";
        }
    }
}