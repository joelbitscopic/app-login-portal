﻿using com.bitscopic.praediauth.dao;
using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace com.bitscopic.praediauth
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LoginPortalSvc : ILoginPortalSvc
    {
        public Stream getSTSSAMLToken(Stream samlTokenRequest)
        {
            try
            {
                // {state:"1L",client_id:"PG_SQA",shared_secret:"ABC123"}
                JObject jobj = SerializerUtils.deserializeFromStream<JObject>(samlTokenRequest);
                String clientId = jobj["client_id"].ToString(); // "PG_SQA";
                String sharedSecret = jobj["shared_secret"].ToString(); //  "ABC123";
                String state = jobj["state"].ToString(); //  "1L";

                LogUtils.Log("Fetching auth service client " + clientId);
                if (String.IsNullOrEmpty(MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId)))
                {
                    LogUtils.Log("Auth service client " + clientId + " not found!");
                    throw new ArgumentException("Invalid client ID: " + clientId);
                }
                String sharedSecretFromConfig = MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId);

                if (false == String.Equals(sharedSecret, sharedSecretFromConfig))
                {
                    LogUtils.Log("Invalid shared secret! '" + sharedSecret + "' does not match client ID: " + clientId);
                    throw new ArgumentException("Invalid shared secret!");
                }
                LogUtils.Log("Client: " + SerializerUtils.serialize(new AuthServiceClient() { clientId = clientId, sharedSecret = MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId) }, false));

                if (String.IsNullOrEmpty(MyConfigurationManager.getValue(clientId + "|" + state))) // use this composite key for state! see code in iam_sso_login where SAML token is set
                {
                    LogUtils.Log("Invalid state! '" + state + "' not found!");
                    throw new ArgumentException("Invalid state!");
                }

                return serializeResult(MyConfigurationManager.getValue(clientId + "|" + state));
            }
            catch (Exception exc)
            {
                return serializeResult(SerializerUtils.serializeExceptionSimple(exc));
            }
        }

        public Stream addNewUser(Stream newBitscopicUserRequest)
        {
            try
            {
                NewBitscopicUserRequest newUser = SerializerUtils.deserializeFromStream<NewBitscopicUserRequest>(newBitscopicUserRequest);
                if (String.IsNullOrEmpty(newUser.email) 
                    || String.IsNullOrEmpty(newUser.firstName) 
                    || String.IsNullOrEmpty(newUser.lastName) 
                    || String.IsNullOrEmpty(newUser.sharedSecret)
                    || String.IsNullOrEmpty(newUser.providersUserId)
                    || String.IsNullOrEmpty(newUser.organization)
                    || newUser.products == null || newUser.products.Count < 1)
                {
                    throw new ArgumentNullException("Missing required fields");
                }

                if (!String.Equals(ConfigurationManager.AppSettings["LoginPortalSvcSharedSecret"], newUser.sharedSecret))
                {
                    throw new ArgumentException("Invalid shared secret");
                }

                // copy these over from email if client doesn't pass - are probably all the same since this API is really just for PIV
                if (String.IsNullOrEmpty(newUser.appUserId))
                {
                    newUser.appUserId = newUser.email;
                }

                using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
                {
                    cxn.Open();
                    SqliteUserDao userDao = new SqliteUserDao(cxn);
                    return serializeResult(userDao.saveNewPIVUser(newUser));
                }
            }
            catch (Exception e)
            {
                return serializeResult(new BitscopicUserTO() { error = new Exception(e.Message) });
            }
        }

        public Stream deleteUser(Stream newBitscopicUserRequest)
        {
            try
            {
                NewBitscopicUserRequest userToDelete = SerializerUtils.deserializeFromStream<NewBitscopicUserRequest>(newBitscopicUserRequest);
                if (String.IsNullOrEmpty(userToDelete.providersUserId) || String.IsNullOrEmpty(userToDelete.sharedSecret))
                {
                    throw new ArgumentNullException("Missing required fields");
                }

                if (!String.Equals(ConfigurationManager.AppSettings["LoginPortalSvcSharedSecret"], userToDelete.sharedSecret))
                {
                    throw new ArgumentException("Invalid shared secret");
                }

                using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
                {
                    cxn.Open();
                    SqliteUserDao userDao = new SqliteUserDao(cxn);
                    userDao.deleteUser(userToDelete);
                }

                return serializeResult("OK");
            }
            catch (Exception e)
            {
                return serializeResult(new BitscopicUserTO() { error = e });
            }
        }

        public Stream googleLoginPost(Stream request)
        {
            try
            {
                JObject deserialized = SerializerUtils.deserializeFromStream<JObject>(request);

                String userEmail = ((String)deserialized["googleUser"]["googleEmail"]).ToLower();
                JToken redirectToken = null;
                String redirectURL = "";
                if (deserialized.TryGetValue("redirectURL", out redirectToken))
                {
                    redirectURL = redirectToken.Value<String>();
                }

                BitscopicUser match = null;
                using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
                {
                    cxn.Open();
                    SqliteUserDao userDao = new SqliteUserDao(cxn);
                    match = userDao.getUserByIdAndIdentityProvider(userEmail, "GOOGLE");
                }

                String jwt = new JWTUtils().getJWT(match); //.getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], match);
                //redirectURL = HttpUtils.getFormattedRedirectURLFromRequest()

                return serializeResult(new BitscopicUserTO(match) { jwt = jwt });
            }
            catch (Exception e)
            {
                return serializeResult(new BitscopicUserTO() { error = e });
            }
        }

        Stream serializeResult(Object obj)
        {
            return SerializerUtils.serializeToStream(obj, false);
        }

        public Stream sayHello(String helloTo)
        {
            return SerializerUtils.serializeToStream("I see you " + helloTo);
        }
    }

    [ServiceContract]
    public interface ILoginPortalSvc
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "samlToken")]
        Stream getSTSSAMLToken(Stream samlTokenRequest);

        [OperationContract]
        [WebInvoke(Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "user")]
        Stream addNewUser(Stream newBitscopicUserRequest);


        [OperationContract]
        [WebInvoke(Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "user")]
        Stream deleteUser(Stream newBitscopicUserRequest);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "googleLogin")]
        Stream googleLoginPost(Stream request);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "hello/{helloTo}")]
        Stream sayHello(String  helloTo);
    }
}
