﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecretBackdoor.aspx.cs" Inherits="com.bitscopic.praediauth.SecretBackdoor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron" style="width: 800px; margin: 0 auto;">

        <div style="width: 40%; margin-left: auto; margin-right: auto; padding-bottom: 20px;">
            <table>
                <tr>
                    <td>
                        <img src="Content/bitscopic-logo.png" alt="Bitscopic" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">Login Portal</td>
                </tr>
            </table>

            
        </div>


        <hr style="border: 1px solid #6d6d6d;" />

        <div id="div_bitscopic_user_login" style="width: 80%; margin-left: auto; margin-right: auto;">

            <table id="table-backdoor-user-login" style="width:90%; margin-left: auto; margin-right: auto;">
                <tr>
                    <td style="color:orangered; font-size: smaller; text-align: center; padding-bottom: 20px" colspan="2">
                        Welcome to the backdoor! It just feels right. Tell us who you want to login as and we'll take care of the rest:         
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: right;">VA&nbsp;Email:&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <asp:TextBox ID="vaEmail" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp</td>
                    <td>
                        <asp:Button CssClass="bitscopic_button" Text="Login" runat="server" OnClick="backdoor_login" />
                    </td>
                </tr>
                <tr style="padding-top: 20px;">
                    <td colspan="2" style="text-align: center"><asp:Label ID="labelMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label></td>
                </tr>
            </table>            
        </div>

    </div>

</asp:Content>
