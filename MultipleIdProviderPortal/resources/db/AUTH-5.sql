CREATE TABLE BITSCOPIC_USER (
    PK         INTEGER  PRIMARY KEY AUTOINCREMENT,
    USER_ID    TEXT     NOT NULL
                        UNIQUE,
    EMAIL      TEXT     NOT NULL,
    LAST_NAME  TEXT     NOT NULL,
    FIRST_NAME TEXT     NOT NULL,
    CREATED    DATETIME NOT NULL
                        DEFAULT (CURRENT_TIMESTAMP),
    ACTIVE     INTEGER  NOT NULL
                        DEFAULT (1) 
);


CREATE TABLE IDENTITY (
    PK                 INTEGER  PRIMARY KEY AUTOINCREMENT,
    BITSCOPIC_USER_PTR INTEGER  NOT NULL,
    IDP                TEXT     NOT NULL,
    IDP_UID            TEXT     NOT NULL,
    CREATED            DATETIME NOT NULL
                                DEFAULT (CURRENT_TIMESTAMP),
    ACTIVE             INTEGER  NOT NULL
                                DEFAULT (1) 
);


CREATE TABLE ACCESS_LOG (
    PK           INTEGER  PRIMARY KEY AUTOINCREMENT,
    IDENTITY_PTR INTEGER  NOT NULL,
    TS           DATETIME NOT NULL
                          DEFAULT (CURRENT_TIMESTAMP),
    CLIENT_ID    TEXT
);