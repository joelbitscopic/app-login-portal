﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using com.bitscopic.praediauth.utils;
using System.Web;
using System.Net.Security;

namespace com.bitscopic.praediauth.domain.iam
{
    public class SecureTokenService
    {
        public string _stsEndpoint = "https://int.services.eauth.va.gov:9301/STS/RequestSecurityToken";
        public String _myAddress = "https://vhapalapppg02.v21.med.va.gov";
        int _waitTimeInSeconds = 15;

        public SecureTokenService() { }

        static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            LogUtils.Log("Validate certificate callback - ignoring any SSL protocol errors");
            return true;
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_IGNORE_SSL_PROTOCOL_ERRORS"))
                && String.Equals(MyConfigurationManager.getValue("IAM_MODEL_IGNORE_SSL_PROTOCOL_ERRORS"), "true", StringComparison.CurrentCultureIgnoreCase))
            {
                LogUtils.Log("Validate certificate callback - ignoring any SSL protocol errors");
                return true;
            }
            return false;
        }

/*        public string getSTSToken(String iamSessionId, Dictionary<String, String> requestHeaders, Dictionary<String, HttpCookie> requestCookies)
        {
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_ENV_ADDRESS")))
            {
                _myAddress = MyConfigurationManager.getValue("IAM_MODEL_ENV_ADDRESS");
            }
            _myAddress = _myAddress + "?userid=" + requestHeaders["secid"] + "&#38;TransactionID=" + requestHeaders["SM_TRANSACTIONID"];
            LogUtils.Log("Using address in SOAP request: " + _myAddress);

            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_STS_API_WAIT_TIME")))
            {
                Int32.TryParse(MyConfigurationManager.getValue("IAM_MODEL_STS_API_WAIT_TIME"), out _waitTimeInSeconds);
            }

            String certificateName = "vhapalapppg02.v21.med.va.gov";//  put the name of your certificate and the appropriate stores here.  it'll look like "CN=blah"
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_CERTIFICATE_NAME")))
            {
                certificateName = MyConfigurationManager.getValue("IAM_MODEL_CERTIFICATE_NAME");
            }


            var soapRequest = "<soap:Envelope xmlns:ns1=\"http://docs.oasis-open.org/ws-sx/ws-trust/200512\" xmlns:wss=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Header/> ";
            soapRequest += "  <soap:Body>   ";
            soapRequest += "      <ns1:RequestSecurityToken> ";
            soapRequest += "       <ns1:OnBehalfOf> ";
            soapRequest += "          <ns1:Base> ";
            soapRequest += "              <wss:BinarySecurityToken EncodingType=\"base64\" ValueType=\"http://ssoi.sts.va.gov/siteminder/std_token\">{0}</wss:BinarySecurityToken> ";
            soapRequest += "          </ns1:Base> ";
            soapRequest += "       </ns1:OnBehalfOf> ";
            soapRequest += "          <wsp:AppliesTo> ";
            soapRequest += "              <wsa:EndpointReference> ";
            soapRequest += "                  <wsa:Address>" + _myAddress + "</wsa:Address> ";
            soapRequest += "              </wsa:EndpointReference> ";
            soapRequest += "          </wsp:AppliesTo> ";
            soapRequest += "          <ns1:Issuer> ";
            soapRequest += "              <wsa:Address>https://ssoi.sts.va.gov/Issuer/smtoken/SAML2</wsa:Address> ";
            soapRequest += "          </ns1:Issuer> ";
            soapRequest += "          <ns1:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Validate</ns1:RequestType> ";
            soapRequest += "          <ns1:TokenType>http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0</ns1:TokenType> ";
            soapRequest += "      </ns1:RequestSecurityToken>   ";
            soapRequest += "  </soap:Body> ";
            soapRequest += "</soap:Envelope> ";


            String requestString = string.Format(soapRequest, iamSessionId);
            LogUtils.Log("Built soap request string: " + requestString);

            //var builtSOAPRequestString = new StringContent(requestString, Encoding.UTF8, "text/xml");

            //model.endpoint = stsEndpoint;
            System.Net.ServicePointManager.Expect100Continue = true;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13 | SecurityProtocolType.Ssl3;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);


            X509Certificate2 clientCert = null;
            if (false == String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_CLIENT_CERT_LOCATION")))
            {
                clientCert = getCertificateFromFile(MyConfigurationManager.getValue("IAM_CLIENT_CERT_LOCATION"));
            }
            else
            {
                clientCert = getCertificateFromStore(StoreName.My, StoreLocation.LocalMachine, certificateName);
            }


            LogUtils.Log("Trying WebRequest...");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_stsEndpoint);
            request.ClientCertificates.Add(clientCert);
            //WebRequest webRequest = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "POST";
            request.ContentType = "text/xml";

            byte[] requestByteAry = System.Text.Encoding.UTF8.GetBytes(requestString);
            request.ContentLength = requestByteAry.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(requestByteAry, 0, requestByteAry.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            LogUtils.Log("Got response! Reading...");
            //setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            String responseBody = rdr.ReadToEnd();
            LogUtils.Log("Response code: " + response.StatusCode.ToString() + " -- Response body: " + responseBody);
            return responseBody;

        }

*/

        public string exchangeOAuthTokenForSAML(String accessToken)
        {
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_STS_ENDPOINT")))
            {
                _stsEndpoint = MyConfigurationManager.getValue("IAM_STS_ENDPOINT");
            }


            String certificateName = "vhapalapppg02.v21.med.va.gov";//  put the name of your certificate and the appropriate stores here.  it'll look like "CN=blah"
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_CERTIFICATE_NAME")))
            {
                certificateName = MyConfigurationManager.getValue("IAM_MODEL_CERTIFICATE_NAME");
            }

            String appURL = "https://vhapalapppg02.v21.med.va.gov/DEV_login_portal/iam_sso_login";
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_REDIRECT_URL")))
            {
                appURL = MyConfigurationManager.getValue("IAM_REDIRECT_URL");
            }

            var soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://docs.oasis-open.org/ws-sx/ws-trust/200512\"> ";
            soapRequest += "	<soapenv:Header/> ";
            soapRequest += "	<soapenv:Body> ";
            soapRequest += "		<ns:RequestSecurityToken> ";
            soapRequest += "			<ns:OnBehalfOf> ";
            soapRequest += "				<ns:Base> ";
            soapRequest += "					<wss:BinarySecurityToken ";
            soapRequest += "						xmlns:wss=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ";
            soapRequest += "						EncodingType=\"http://ibm.com/2004/01/itfim/base64encode\" ";
            soapRequest += "						ValueType=\"urn:ibm:names:ISAM:oauth:param\">" + accessToken + "</wss:BinarySecurityToken> ";
            soapRequest += "				</ns:Base> ";
            soapRequest += "			</ns:OnBehalfOf> ";
            soapRequest += "			<wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\"> ";
            soapRequest += "				<wsa:EndpointReference xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\"> ";
            soapRequest += "					<wsa:Address>" + appURL + "</wsa:Address> ";
            soapRequest += "				</wsa:EndpointReference> ";
            soapRequest += "			</wsp:AppliesTo> ";
            soapRequest += "			<ns:Issuer xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\"> ";
            soapRequest += "				<wsa:Address xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">https://sts.va.gov/Issuer/smtoken/SAML2</wsa:Address> ";
            soapRequest += "			</ns:Issuer> ";
            soapRequest += "		</ns:RequestSecurityToken> ";
            soapRequest += "	</soapenv:Body> ";
            soapRequest += "</soapenv:Envelope> ";



            String requestString = soapRequest; // string.Format(soapRequest, iamSessionId);
            LogUtils.Log("Built soap request string: " + requestString);

            //var builtSOAPRequestString = new StringContent(requestString, Encoding.UTF8, "text/xml");

            //model.endpoint = stsEndpoint;
            System.Net.ServicePointManager.Expect100Continue = true;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);


            X509Certificate2 clientCert = null;
            //if (false == String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_CLIENT_CERT_LOCATION")))
            //{
            //    clientCert = getCertificateFromFile(MyConfigurationManager.getValue("IAM_CLIENT_CERT_LOCATION"));
            //}
            //else
            //{
                clientCert = getCertificateFromStore(StoreName.My, StoreLocation.LocalMachine, certificateName);
            //}

            LogUtils.Log("Trying WebRequest to " + _stsEndpoint);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_stsEndpoint);
                request.ClientCertificates.Add(clientCert);
                //WebRequest webRequest = WebRequest.Create(String.Concat(baseUri, resource));
                request.Method = "POST";
                request.ContentType = "text/xml";

                byte[] requestByteAry = System.Text.Encoding.UTF8.GetBytes(requestString);
                request.ContentLength = requestByteAry.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(requestByteAry, 0, requestByteAry.Length);
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                LogUtils.Log("Got response! Reading...");
                //setMyHeadersFromResponse(response);
                Stream stream = response.GetResponseStream();
                StreamReader rdr = new StreamReader(stream);

                String responseBody = rdr.ReadToEnd();
                LogUtils.Log("Response code: " + response.StatusCode.ToString() + " -- Response body: " + responseBody);
                return responseBody;
            }
            catch (Exception exc)
            {
                LogUtils.Log("Exception: " + SerializerUtils.serializeExceptionSimple(exc, 2));
                throw exc;
            }
        }

        public static X509Certificate2 getCertificateFromStore(StoreName name, StoreLocation location, string subjectName)
        {
            X509Store store = new X509Store(name, location);
            X509Certificate2Collection certificates = null;
            store.Open(OpenFlags.ReadOnly);

            try
            {
                X509Certificate2 result = null;
                certificates = store.Certificates;

             //   X509Certificate2 exportedCert = new X509Certificate2("C:\\users\\joel\\desktop\\vhapalapppg02.v21.med.va.gov.cer");

                for (int i = 0; i < certificates.Count; i++)
                {
                    X509Certificate2 cert = certificates[i];
                    //FileLog.LogMessage(Category.Debug, "Certificate Subject Name: " + cert.SubjectName.Name.ToString());
                    LogUtils.Log(String.Format("Found certificate name in store! Subject: {0} | Subject Name: {1}", cert.Subject, cert.SubjectName.Name));

                    if (false == String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_MODEL_USE_CERT_IDX")))
                    {
                        if (String.Equals(i.ToString(), MyConfigurationManager.getValue("IAM_MODEL_USE_CERT_IDX")))
                        {
                            LogUtils.Log(string.Format("Using cert {0} in index position {1}", subjectName, MyConfigurationManager.getValue("IAM_MODEL_USE_CERT_IDX")));
                            return new X509Certificate2(cert);
                        }
                        else
                        {
                            LogUtils.Log(string.Format("Found cert {0} in index position {1} but NOT using since IAM_MODEL_USE_CERT_IDX is set and doesn't match...", subjectName, i.ToString()));
                            continue;
                        }
                    }

                    if (String.Equals(cert.SubjectName.Name.ToString(), subjectName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (result != null)
                        {
                            //throw new ApplicationException(string.Format("There are multiple certificate for subject Name {0}", subjectName));
                            LogUtils.Log(string.Format("There are multiple certificate for subject Name {0}", subjectName));
                            continue;
                        }
                        result = new X509Certificate2(cert);
                    }
                }
                if (result == null)
                {
                    throw new ApplicationException(string.Format("No certificate was found for subject Name {0}", subjectName));
                }
                return result;
            }
            finally
            {
                if (certificates != null)
                {
                    for (int i = 0; i < certificates.Count; i++)
                    {
                        X509Certificate2 cert = certificates[i];
                        cert.Reset();
                    }
                }
                store.Close();
            }
        }

        public static X509Certificate2 getCertificateFromFile(String filePath)
        {
            LogUtils.Log("Loading cert from file: " + filePath);
            X509Certificate2 result = new X509Certificate2(filePath);
            //System.Console.WriteLine("Cert Subject: " + result.Subject + " -- Friendly Name: " + result.FriendlyName);
            LogUtils.Log("Successfully loaded cert from file: " + result.Subject);
            return result;
        }
    }

}