﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace com.bitscopic.praediauth.domain.google
{
    [Serializable]
    public class GooglePlusAccessToken
    {
        public String access_token;
        public String access_type;
        public int expires_in;
        public String id_token;
        public String refresh_token;
    }
}