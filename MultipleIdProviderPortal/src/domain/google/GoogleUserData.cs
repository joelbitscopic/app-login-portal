﻿using System;
using Newtonsoft.Json;

namespace com.bitscopic.praediauth.domain.google
{
    [Serializable]
    public class GoogleUserData
    {
        public String id;
        public String name;
        public String given_name;
        public String email;
        public String picture;
    }
}