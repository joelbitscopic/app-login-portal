﻿using System;
using System.Collections.Generic;

namespace com.bitscopic.praediauth.domain
{
    public class ClientState
    {
        public string state;
        public string redirect_uri;
        public Dictionary<String, String> flags;
        public string clientId;
        public Dictionary<String, String> assertedIdentities;

        public ClientState() { }

        public ClientState(String state, String redirectUrl)
        {
            this.state = state;
            this.redirect_uri = redirectUrl;
        }
    }
}