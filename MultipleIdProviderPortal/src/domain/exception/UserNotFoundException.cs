﻿using System;

namespace com.bitscopic.praediauth.domain.exception
{
    public class UserNotFoundException : ApplicationException
    {
        public UserNotFoundException() : base() { }

        public UserNotFoundException(String message) : base(message) { }
    }
}