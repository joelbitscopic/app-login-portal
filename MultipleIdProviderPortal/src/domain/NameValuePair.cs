﻿using System;

namespace com.bitscopic.praediauth.domain
{
    [Serializable]
    public class NameValuePair
    {
        public NameValuePair() { }

        public NameValuePair(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String name;
        public String value;
    }
}