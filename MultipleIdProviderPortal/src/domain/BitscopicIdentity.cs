﻿using System;
using System.Collections.Generic;

namespace com.bitscopic.praediauth.domain
{
    public class BitscopicIdentity
    {
        public String bitscopic_id;
        public String email;
        public String given_name;
        public String family_name;
        public List<String> aliases;
        public List<AccessToken> accessTokens;

        public BitscopicIdentity() { }
    }
}