﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace com.bitscopic.praediauth.domain
{
    public static class MyConfigurationManager
    {
        // case insensitive config dict
        internal static Dictionary<String, Object> configsByKey = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// Clear the current configs and reload
        /// </summary>
        internal static void reloadConfigs()
        {
            configsByKey = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            loadConfigs();
        }


        internal static void loadConfigs()
        {
            String[] configKeys = System.Configuration.ConfigurationManager.AppSettings.AllKeys;
            foreach (String configKey in configKeys)
            {
                if (!configsByKey.ContainsKey(configKey))
                {
                    configsByKey.Add(configKey, System.Configuration.ConfigurationManager.AppSettings[configKey]);
                }
            }
        }

        public static String getValue(String configKey)
        {
            if (configsByKey.Count == 0)
            {
                loadConfigs();
            }

            if (configsByKey.ContainsKey(configKey))
            {
                return (String)configsByKey[configKey];
            }
            else
            {
                return String.Empty;
            }
        }

        public static void setValue(string key, string value)
        {
            if (configsByKey == null || configsByKey.Count == 0)
            {
                reloadConfigs();
            }

            if (configsByKey.ContainsKey(key))
            {
                configsByKey[key] = value;
            }
            else
            {
                configsByKey.Add(key, value);
            }
        }
    }

}