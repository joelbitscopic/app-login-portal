﻿using System;

namespace com.bitscopic.praediauth.domain
{
    public class IDPState
    {
        public string state;
        public string redirect_uri;
        public object idpIdentity;
        public string client_state;

        public IDPState() { }

        public IDPState(String state, String redirectUrl)
        {
            this.state = state;
            this.redirect_uri = redirectUrl;
        }
    }
}