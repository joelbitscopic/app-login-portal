﻿using System;
using System.Collections.Generic;

namespace com.bitscopic.praediauth.domain
{
    [Serializable]
    public class BitscopicUser
    {
        public BitscopicUser() { }

        public String dbKey;
        public String lastName;
        public String firstName;
        public String email;
        public String id;
        public String role;
        public string providersUserId;
        public string appUserId;
        public List<string> products;
        public string organization;

        public List<Identity> identities;
        public List<AccessToken> accessTokens;
    }

    public class Identity
    {
        public Identity() { }

        public String dbKey;
        public String id;
        public IdentityProvider provider;
    }

    public enum IdentityProvider
    {
        VA_IAM = 1,
        VA_EMAIL = 2,
        BITSCOPIC = 3
    }

    public class AccessToken
    {
        public AccessToken() { }

        public AccessTokenType type;
        public String value;
        public AccessTokenIssuer issuer;
    }

    public enum AccessTokenType
    {
        ACCESS_TOKEN = 1,
        REFRESH_TOKEN = 2
    }
    public enum AccessTokenIssuer
    {
        VA_IAM = 1
    }



    [Serializable]
    public class BitscopicUserTO : TO
    {
        public BitscopicUserTO() { }

        public BitscopicUserTO(BitscopicUser user)
        {
            this.appUserId = user.appUserId;
            this.email = user.email;
            this.firstName = user.firstName;
            this.id = user.id;
            this.lastName = user.lastName;
            this.providersUserId = user.providersUserId;
            this.role = user.role;
            this.organization = user.organization;
            this.products = user.products;
        }

        public String lastName;
        public String firstName;
        public String email;
        public String id;
        public String role;
        public string providersUserId;
        public string appUserId;
        public String jwt;
        public List<string> products;
        public string organization;
    }

    [Serializable]
    public class NewBitscopicUserRequest : BitscopicUser
    {
        public NewBitscopicUserRequest() { }

        public String sharedSecret;
    }
}