﻿using com.bitscopic.praediauth.domain;
using System;

namespace com.bitscopic.praediauth.dao.sql
{
    public class SqlConnectionFactory
    {
        public static ISqlConnection getConnection()
        {
            String provider = MyConfigurationManager.getValue("USER_DB_PROVIDER");
            String cxnString = MyConfigurationManager.getValue("USER_DB_CONNECTION_STRING");
            if (String.IsNullOrEmpty(provider))
            {
                provider = "SQLITE";
            }
            SourceSystemType providerType = (SourceSystemType)Enum.Parse(typeof(SourceSystemType), provider, true);

            if (String.IsNullOrEmpty(cxnString))
            {
                throw new ArgumentException("No DB connection string found in config");
            }

            if (providerType == domain.SourceSystemType.SQLITE)
            {
                return new com.bitscopic.praediauth.dao.sql.SqliteConnection(new SourceSystem() { connectionString = cxnString, type = SourceSystemType.SQLITE });
            }
            else
            {
                throw new ArgumentException(Enum.GetName(typeof(SourceSystemType), providerType) + " is not currently a valid SQL source system type");
            }
        }

        public static ISqlConnection getConnectionBySource(SourceSystem ss)
        {
            if (ss.type == domain.SourceSystemType.SQLITE)
            {
                return new com.bitscopic.praediauth.dao.sql.SqliteConnection(ss);
            }
            else
            {
                throw new ArgumentException(Enum.GetName(typeof(SourceSystemType), ss.type) + " is not currently a valid SQL source system type");
            }
        }
    }
}