﻿using com.bitscopic.praediauth.domain;
using System;

namespace com.bitscopic.praediauth.dao.sql
{
    public interface IBitscopicUserDao
    {
        BitscopicUser getUser(String bitscopicUserId);
        void logAccess(String clientId, String identityDbKey);
        Identity addIdentity(String bitscopicUserDbKey, Identity identity);
        BitscopicUser createBitscopicUser(BitscopicUser user);
        BitscopicUser getUserByIdentity(IdentityProvider idp, String idpUserId);
    }
}
