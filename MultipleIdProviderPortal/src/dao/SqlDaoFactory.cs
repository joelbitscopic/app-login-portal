﻿using com.bitscopic.praediauth.dao.sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.bitscopic.praediauth.dao.sql
{
    public class SqlDaoFactory
    {
        public static IBitscopicUserDao getBitscopicUserDao(ISqlConnection cxn)
        {
            return new SqliteBitscopicUserDao(cxn);
        }
    }
}