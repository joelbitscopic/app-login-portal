﻿using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.domain.exception;
using System;
using System.Data;
using System.Data.SQLite;

namespace com.bitscopic.praediauth.dao.sql
{
    public class SqliteBitscopicUserDao : IBitscopicUserDao
    {
        SqliteConnection _cxn;

        public SqliteBitscopicUserDao(ISqlConnection cxn) 
        {
            if (false == (cxn is SqliteConnection))
            {
                throw new ArgumentException("Invalid connection type. Not a SQLite connection!");
            }
            _cxn = (SqliteConnection)cxn;
        }

        public BitscopicUser getUser(String bitscopicUserId)
        {
            using (IDbCommand cmd = buildGetUserCmd(bitscopicUserId))
            {
                using (IDataReader rdr = _cxn.select(cmd))
                {
                    if (false == rdr.Read())
                    {
                        throw new UserNotFoundException("User not found: " + bitscopicUserId);
                    }
                    return toBitscopicUserWithIdentity(rdr);
                }
            }
        }

        public void logAccess(String clientId, String identityDbKey)
        {
            using (IDbCommand cmd = _cxn.buildCommand())
            {
                cmd.CommandText = "INSERT INTO ACCESS_LOG (CLIENT_ID, IDENTITY_PTR) VALUES (:clientId, :idPtr)";
                cmd.Parameters.Add(new SQLiteParameter(":clientId", clientId));
                cmd.Parameters.Add(new SQLiteParameter(":idPtr", Convert.ToInt32(identityDbKey)));
                _cxn.insertUpdateDelete(cmd);
            }
        }

        internal IDbCommand buildGetUserCmd(String bitscopicUserId)
        {
            String sql = "SELECT USER.PK AS BITSCOPIC_USER_PK, USER.USER_ID AS BITSCOPIC_USER_ID, USER.EMAIL AS BITSCOPIC_USER_EMAIL, "
                + " USER.LAST_NAME AS BITSCOPIC_USER_LAST_NAME, USER.FIRST_NAME AS BITSCOPIC_USER_FIRST_NAME, "
                + " ID.PK AS IDENTITY_PK, ID.IDP AS IDENTITY_IDP, ID.IDP_UID AS IDENTITY_IDP_UID "
                + " FROM BITSCOPIC_USER USER JOIN IDENTITY ID ON USER.PK=ID.BITSCOPIC_USER_PTR " 
                + " WHERE USER.ACTIVE=1 AND ID.ACTIVE=1 AND USER.USER_ID=:uid";
            IDbCommand cmd = _cxn.buildCommand();
            cmd.CommandText = sql;

            cmd.Parameters.Add(new SQLiteParameter(":uid", bitscopicUserId));

            return cmd;
        }

        //internal BitscopicUser toBitscopicUser(IDataReader reader)
        //{
        //    BitscopicUser result = new BitscopicUser();
        //    result.dbKey = Convert.ToString(SqlUtils.safeGetInt(reader, "BITSCOPIC_USER_PK", _cxn));
        //    result.id = SqlUtils.safeGet(reader, "BITSCOPIC_USER_ID");
        //    result.email = SqlUtils.safeGet(reader, "BITSCOPIC_USER_EMAIL");
        //    result.lastName = SqlUtils.safeGet(reader, "BITSCOPIC_USER_LAST_NAME");
        //    result.firstName = SqlUtils.safeGet(reader, "BITSCOPIC_USER_FIRST_NAME");
        //    return result;
        //}

        internal BitscopicUser toBitscopicUserWithIdentity(IDataReader reader)
        {
            BitscopicUser result = new BitscopicUser();
            result.dbKey = Convert.ToString(SqlUtils.safeGetInt(reader, "BITSCOPIC_USER_PK", _cxn));
            result.id = SqlUtils.safeGet(reader, "BITSCOPIC_USER_ID");
            result.email = SqlUtils.safeGet(reader, "BITSCOPIC_USER_EMAIL");
            result.lastName = SqlUtils.safeGet(reader, "BITSCOPIC_USER_LAST_NAME");
            result.firstName = SqlUtils.safeGet(reader, "BITSCOPIC_USER_FIRST_NAME");

            Identity first = new Identity();
            first.dbKey = Convert.ToString(SqlUtils.safeGetInt(reader, "IDENTITY_PK", _cxn));
            first.provider = (IdentityProvider)Enum.Parse(typeof(IdentityProvider), SqlUtils.safeGet(reader, "IDENTITY_IDP"));
            first.id = SqlUtils.safeGet(reader, "IDENTITY_IDP_UID");
            result.identities = new System.Collections.Generic.List<Identity>() { first };
            while (reader.Read())
            {
                Identity current = new Identity();
                current.dbKey = Convert.ToString(SqlUtils.safeGetInt(reader, "IDENTITY_PK", _cxn));
                current.provider = (IdentityProvider)Enum.Parse(typeof(IdentityProvider), SqlUtils.safeGet(reader, "IDENTITY_IDP"));
                current.id = SqlUtils.safeGet(reader, "IDENTITY_IDP_UID");
                result.identities.Add(current);
            }
            return result;
        }

        public Identity addIdentity(String bitscopicUserDbKey, Identity identity)
        {
            using (IDbCommand cmd = buildAddIdentityCmd(bitscopicUserDbKey, identity))
            {
                identity.dbKey = _cxn.insertReturningRowId(cmd);
            }

            return identity;
        }

        public BitscopicUser createBitscopicUser(BitscopicUser user)
        {
            IDbTransaction tx = _cxn.startTransaction();

            try
            {
                using (IDbCommand cmd = buildCreateBitscopicUserCmd(user))
                {
                    cmd.Transaction = tx;
                    user.dbKey = _cxn.insertReturningRowId(cmd);
                }
                foreach (Identity id in user.identities)
                {
                    using (IDbCommand cmd = buildAddIdentityCmd(user.dbKey, id))
                    {
                        cmd.Transaction = tx;
                        id.dbKey = _cxn.insertReturningRowId(cmd);
                    }
                }

                _cxn.commitTransaction(tx);
            }
            catch (Exception)
            {
                _cxn.rollbackTransaction(tx);
                throw;
            }

            return user;
        }

        internal IDbCommand buildCreateBitscopicUserCmd(BitscopicUser user)
        {
            String sql = "INSERT INTO BITSCOPIC_USER (USER_ID, EMAIL, LAST_NAME, FIRST_NAME) VALUES (:id, :email, :lastName, :firstName)";

            IDbCommand cmd = _cxn.buildCommand();
            cmd.CommandText = sql;

            cmd.Parameters.Add(new SQLiteParameter(":id", user.id));
            cmd.Parameters.Add(new SQLiteParameter(":email", String.IsNullOrEmpty(user.email) ? "<unknown>" : user.email));
            cmd.Parameters.Add(new SQLiteParameter(":lastName", user.lastName));
            cmd.Parameters.Add(new SQLiteParameter(":firstName", user.firstName));

            return cmd;
        }

        internal IDbCommand buildAddIdentityCmd(String bitscopidUserDbKey, Identity identity)
        {
            String sql = "INSERT INTO IDENTITY (BITSCOPIC_USER_PTR, IDP, IDP_UID) VALUES (:ptr, :idp, :idpUserId)";

            IDbCommand cmd = _cxn.buildCommand();
            cmd.CommandText = sql;

            cmd.Parameters.Add(new SQLiteParameter(":ptr", Convert.ToInt32(bitscopidUserDbKey)));
            cmd.Parameters.Add(new SQLiteParameter(":idp", Enum.GetName(typeof(IdentityProvider), identity.provider)));
            cmd.Parameters.Add(new SQLiteParameter(":idpUserId", identity.id));

            return cmd;
        }


        public BitscopicUser getUserByIdentity(IdentityProvider idp, String idpUserId)
        {
            using (IDbCommand cmd = buildGetUserByIdentityCmd(idp, idpUserId))
            {
                using (IDataReader rdr = _cxn.select(cmd))
                {
                    if (false == rdr.Read())
                    {
                        throw new UserNotFoundException("User not found for identity provider/id: " + Enum.GetName(typeof(IdentityProvider), idp) + "/" + idpUserId);
                    }
                    return toBitscopicUserWithIdentity(rdr);
                }
            }
        }

        internal IDbCommand buildGetUserByIdentityCmd(IdentityProvider idp, String idpUserId)
        {
            String sql = "SELECT USER.PK AS BITSCOPIC_USER_PK, USER.USER_ID AS BITSCOPIC_USER_ID, USER.EMAIL AS BITSCOPIC_USER_EMAIL, "
            + " USER.LAST_NAME AS BITSCOPIC_USER_LAST_NAME, USER.FIRST_NAME AS BITSCOPIC_USER_FIRST_NAME, "
            + " ID.PK AS IDENTITY_PK, ID.IDP AS IDENTITY_IDP, ID.IDP_UID AS IDENTITY_IDP_UID "
            + " FROM BITSCOPIC_USER USER JOIN IDENTITY ID ON USER.PK=ID.BITSCOPIC_USER_PTR "
            + " WHERE USER.ACTIVE=1 AND ID.ACTIVE=1 AND ID.IDP=:idp AND ID.IDP_UID=:idpUserId";
            IDbCommand cmd = _cxn.buildCommand();
            cmd.CommandText = sql;

            cmd.Parameters.Add(new SQLiteParameter(":idp", Enum.GetName(typeof(IdentityProvider), idp)));
            cmd.Parameters.Add(new SQLiteParameter(":idpUserId", idpUserId));

            return cmd;


        }
    }
}