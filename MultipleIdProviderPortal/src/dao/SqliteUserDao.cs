﻿using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;

namespace com.bitscopic.praediauth.dao
{
    public class SqliteUserDao
    {
        SQLiteConnection _cxn;

        public SqliteUserDao(SQLiteConnection cxn)
        {
            _cxn = cxn;
        }

        public BitscopicUser addBitscopicUser(BitscopicUser user)
        {
            throw new NotImplementedException();
            IDbTransaction tx = _cxn.BeginTransaction();
            try
            {
                IDbCommand addUserCmd = buildAddUserCmd(user);

                tx.Commit();
            }
            catch (Exception)
            {
                tx.Rollback();
                throw;
            }
            finally
            {

            }
        }

        internal IDbCommand buildAddUserCmd(BitscopicUser user)
        {
            SQLiteTransaction tx = _cxn.BeginTransaction();

            IDbCommand bitscopicUserCmd = _cxn.CreateCommand();
            bitscopicUserCmd.CommandText = "INSERT INTO BITSCOPIC_USER (USER_ID, EMAIL, LAST_NAME, FIRST_NAME) VALUES (:userId, :email, :lastName, :firstName)";

            bitscopicUserCmd.Parameters.Add(new SQLiteParameter("userId", user.id));
            bitscopicUserCmd.Parameters.Add(new SQLiteParameter("email", user.email));
            bitscopicUserCmd.Parameters.Add(new SQLiteParameter("lastName", user.lastName));
            bitscopicUserCmd.Parameters.Add(new SQLiteParameter("firstName", user.firstName));

            return bitscopicUserCmd;
        }

        internal IDbCommand buildAddUserIdentityCmd(String userDbKey, Identity identity)
        {
            SQLiteTransaction tx = _cxn.BeginTransaction();

            IDbCommand identityCmd = _cxn.CreateCommand();
            identityCmd.CommandText = "INSERT INTO IDENTITY (BITSCOPIC_USER_PTR, IDP, IDP_UID) VALUES (:ptr, :idp, :idpUid)";

            identityCmd.Parameters.Add(new SQLiteParameter("ptr", userDbKey));
            identityCmd.Parameters.Add(new SQLiteParameter("idp", identity.provider));
            identityCmd.Parameters.Add(new SQLiteParameter("idpUid", identity.id));

            return identityCmd;
        }

        public void deleteUser(BitscopicUser user)
        {
            using (IDbCommand cmd = buildDeactivateUserCmd(user)) 
            {
                if (1 != cmd.ExecuteNonQuery())
                {
                    throw new ArgumentException("No records affected!");
                }
            }
        }

        internal IDbCommand buildDeactivateUserCmd(BitscopicUser user)
        {
            IDbCommand cmd = _cxn.CreateCommand();

            cmd.CommandText = "DELETE FROM USER WHERE USER_ID=:userId";

            cmd.Parameters.Add(new SQLiteParameter("userId", user.providersUserId));

            return cmd;
        }

        public BitscopicUser saveNewUser(BitscopicUser user, String idProvider)
        {
            using (IDbCommand cmd = buildSaveNewUserCmd(user, idProvider, "PRAEDICO")) // using these defaults as i don't think they currently matter
            {
                cmd.ExecuteNonQuery();
            }

            using (IDbCommand fetchLastInsertRowId = _cxn.CreateCommand())
            {
                fetchLastInsertRowId.CommandText = "SELECT last_insert_rowid()";
                user.id = Convert.ToString(fetchLastInsertRowId.ExecuteScalar());
                return user;
            }
        }

        public BitscopicUser saveNewPIVUser(BitscopicUser user)
        {
            return saveNewUser(user, "PIV");
        }

        internal IDbCommand buildSaveNewUserCmd(BitscopicUser user, String idProviderType, String appName)
        {
            IDbCommand cmd = _cxn.CreateCommand();

            cmd.CommandText = "INSERT INTO USER (USER_ID, PROVIDER, APP_NAME, USERS_APP_ID, USER_OBJ_JSON) VALUES (:userId, :provider, :appName, :userAppId, :json)";

            cmd.Parameters.Add(new SQLiteParameter("userId", user.providersUserId));
            cmd.Parameters.Add(new SQLiteParameter("provider", idProviderType));
            cmd.Parameters.Add(new SQLiteParameter("appName", String.IsNullOrEmpty(appName) ? "PRAEDICO" : appName));
            cmd.Parameters.Add(new SQLiteParameter("userAppId", user.appUserId));

            //List<NameValuePair> nvps = new List<NameValuePair>();
            //nvps.Add(new NameValuePair("firstName", user.firstName));
            //nvps.Add(new NameValuePair("lastName", user.lastName));
            //nvps.Add(new NameValuePair("role", "ACCOUNTUSER"));
            //nvps.Add(new NameValuePair("email", user.email));
            BitscopicUser userForJSON = new BitscopicUser();
            userForJSON.firstName = user.firstName;
            userForJSON.lastName = user.lastName;
            userForJSON.role = user.role;
            userForJSON.email = user.email;
            userForJSON.organization = user.organization;
            userForJSON.products = user.products;

            cmd.Parameters.Add(new SQLiteParameter("json", SerializerUtils.serialize(userForJSON)));

            return cmd;
        }

        public BitscopicUser getUserByIdAndIdentityProvider(String userId, String identityProvider)
        {
            using (IDbCommand cmd = buildGetUserByIdAndIdentityProviderCmd(userId, identityProvider))
            {
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        throw new ArgumentException("User not found");
                    }
                    return toUser(rdr);
                }
            }
        }

        internal IDbCommand buildGetUserByIdAndIdentityProviderCmd(String userId, String identityProvider)
        {
            IDbCommand cmd = _cxn.CreateCommand();

            cmd.CommandText = "SELECT * FROM USER WHERE USER_ID=:userId AND PROVIDER=:idProvider AND ACTIVE=1";

            IDbDataParameter userIdParam = new SQLiteParameter("userId");
            userIdParam.Value = userId;
            cmd.Parameters.Add(userIdParam);

            IDbDataParameter idProviderParam = new SQLiteParameter("idProvider");
            idProviderParam.Value = identityProvider;
            cmd.Parameters.Add(idProviderParam);

            return cmd;
        }

        public BitscopicUser getUserByIdAndIdentityProviderForApp(String userId, String identityProvider, String appName)
        {
            using (IDbCommand cmd = buildGetUserByIdAndIdentityProviderForAppCmd(userId, identityProvider, appName))
            {
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        throw new ArgumentException("No data found!");
                    }

                    return toUser(rdr);
                }
            }
        }

        internal IDbCommand buildGetUserByIdAndIdentityProviderForAppCmd(String userId, String identityProvider, String appName)
        {
            IDbCommand cmd = _cxn.CreateCommand();

            cmd.CommandText = "SELECT * FROM USER WHERE USER_ID=:userId AND PROVIDER=:idProvider AND APP_NAME=:appName AND ACTIVE=1";

            IDbDataParameter userIdParam = new SQLiteParameter("userId");
            userIdParam.Value = userId;
            cmd.Parameters.Add(userIdParam);

            IDbDataParameter idProviderParam = new SQLiteParameter("idProvider");
            idProviderParam.Value = identityProvider;
            cmd.Parameters.Add(idProviderParam);

            IDbDataParameter appNameParam = new SQLiteParameter("appName");
            appNameParam.Value = appName;
            cmd.Parameters.Add(appNameParam);

            return cmd;
        }

        public BitscopicUser getUserByIdAndPassword(String username, String plaintextPassword)
        {
            using (IDbCommand cmd = buildGetUserByIdAndPasswordCmd(username, plaintextPassword))
            {
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        return null;
                    }
                    else
                    {
                        return toUser(rdr);
                    }
                }
            }
        }

        internal IDbCommand buildGetUserByIdAndPasswordCmd(String userId, String plaintextPassword)
        {
            IDbCommand cmd = _cxn.CreateCommand();

            cmd.CommandText = "SELECT * FROM USER WHERE USER_ID=:userId AND PASSWORD=:pwd AND ACTIVE=1";

            IDbDataParameter userIdParam = new SQLiteParameter("userId");
            userIdParam.Value = userId;
            cmd.Parameters.Add(userIdParam);

            IDbDataParameter pwdParam = new SQLiteParameter("pwd");
            pwdParam.Value = CryptoUtils.hash(plaintextPassword);
            cmd.Parameters.Add(pwdParam);

            return cmd;
        }

        internal List<BitscopicUser> toUsers(IDataReader rdr)
        {
            List<BitscopicUser> result = new List<BitscopicUser>();
            while (rdr.Read())
            {
                result.Add(toUser(rdr));
            }
            return result;
        }

        internal BitscopicUser toUser(IDataReader rdr)
        {
            BitscopicUser result = new BitscopicUser();
            result.id = Convert.ToString(rdr.GetInt32(rdr.GetOrdinal("PK")));
            result.providersUserId = rdr.GetString(rdr.GetOrdinal("USER_ID"));
            result.appUserId = rdr.GetString(rdr.GetOrdinal("USERS_APP_ID"));

            BitscopicUser userObjDeserialized = SerializerUtils.deserialize<BitscopicUser>(rdr.GetString(rdr.GetOrdinal("USER_OBJ_JSON")));
            result.email = userObjDeserialized.email;
            result.firstName = userObjDeserialized.firstName;
            result.lastName = userObjDeserialized.lastName;
            result.role = userObjDeserialized.role;
            result.organization = userObjDeserialized.organization;
            result.products = userObjDeserialized.products;

            //List<NameValuePair> userProps = SerializerUtils.deserialize<List<NameValuePair>>(rdr.GetString(rdr.GetOrdinal("USER_OBJ_JSON")));
            //if (userProps.Any(nvp => String.Equals(nvp.name, "email", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.email = userProps.First(nvp => String.Equals(nvp.name, "email", StringComparison.CurrentCultureIgnoreCase)).value;
            //}

            //if (userProps.Any(nvp => String.Equals(nvp.name, "firstName", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.firstName = userProps.First(nvp => String.Equals(nvp.name, "firstName", StringComparison.CurrentCultureIgnoreCase)).value;
            //}

            //if (userProps.Any(nvp => String.Equals(nvp.name, "lastName", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.lastName = userProps.First(nvp => String.Equals(nvp.name, "lastName", StringComparison.CurrentCultureIgnoreCase)).value;
            //}

            //if (userProps.Any(nvp => String.Equals(nvp.name, "role", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.role = userProps.First(nvp => String.Equals(nvp.name, "role", StringComparison.CurrentCultureIgnoreCase)).value;
            //}

            //if (userProps.Any(nvp => String.Equals(nvp.name, "organization", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.organization = userProps.First(nvp => String.Equals(nvp.name, "organization", StringComparison.CurrentCultureIgnoreCase)).value;
            //}

            //if (userProps.Any(nvp => String.Equals(nvp.name, "products", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    result.products = SerializerUtils.deserialize<List<String>>(userProps.First(nvp => String.Equals(nvp.name, "products", StringComparison.CurrentCultureIgnoreCase)).value);
            //}

            return result;
        }

    }
}