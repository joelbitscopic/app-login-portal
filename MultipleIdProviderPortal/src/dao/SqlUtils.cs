﻿using System;
using System.Data;
using System.Collections.Generic;

namespace com.bitscopic.praediauth.dao.sql
{
    public static class SqlUtils
    {

        /// <summary>
        /// Adjust a string for direct insert in to a SQL database. For example, escape single quote with an additional single quote
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal static string adjustStringForSql(string s, bool wrapWithSingleQuote = false)
        {
            String result = s.Replace("'", "''");

            if (wrapWithSingleQuote)
            {
                result = String.Concat("'", result, "'");
            }

            return result;
        }

        /// <summary>
        /// Fetch all values in a column
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        internal static List<string> getColumnFromReader(IDataReader rdr, String columnName)
        {
            List<String> result = new List<String>();

            Int32 colPos = -1;
            while (rdr.Read())
            {
                if (colPos < 0)
                {
                    colPos = rdr.GetOrdinal(columnName);
                }

                result.Add(rdr.GetString(colPos));
            }

            return result;
        }

        /// <summary>
        /// Fetch all values in a column
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        internal static List<T> getColumnFromDataTable<T>(DataTable table, String columnName)
        {
            if (!table.Columns.Contains(columnName))
            {
                throw new ArgumentException("Table does not contain a column named: " + columnName);
            }
            Int32 colPos = table.Columns.IndexOf(columnName);

            List<T> result = new List<T>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                result.Add((T)table.Rows[i][colPos]);
            }

            return result;
        }

        internal static Double safeGetDouble(IDataReader rdr, String columnName, ISqlConnection cxn)
        {
            Int32 colIdx = rdr.GetOrdinal(columnName);
            if (colIdx < 0 || rdr.IsDBNull(colIdx))
            {
                return 0;
            }

            if (String.Equals("SQLite", cxn.getProvider(), StringComparison.CurrentCultureIgnoreCase))
            {
                if (rdr[colIdx].GetType() == typeof(Double))
                {
                    return (Double)rdr.GetValue(colIdx);
                }
                else if (rdr[colIdx].GetType() == typeof(String))
                {
                    return Convert.ToDouble((String)rdr.GetValue(colIdx));
                }
                else if (rdr[colIdx].GetType() == typeof(int))
                {
                    return Convert.ToDouble((Int64)rdr.GetValue(colIdx));
                }
            }
            else if (String.Equals("MSSQL", cxn.getProvider(), StringComparison.CurrentCultureIgnoreCase))
            {
                return Convert.ToDouble((Decimal)rdr.GetValue(colIdx));
            }

            throw new NotImplementedException("Double fetching/parsing has for " + cxn.getProvider() + " has not been implemented");
        }

        internal static Int64 safeGetInt(IDataReader rdr, String columnName, ISqlConnection cxn)
        {
            Int32 colIdx = rdr.GetOrdinal(columnName);
            if (colIdx < 0 || rdr.IsDBNull(colIdx))
            {
                return 0;
            }

            if (String.Equals("SQLite", cxn.getProvider(), StringComparison.CurrentCultureIgnoreCase))
            {
                return Convert.ToInt64(rdr.GetValue(colIdx));
            }
            else if (String.Equals("MSSQL", cxn.getProvider(), StringComparison.CurrentCultureIgnoreCase))
            {
                return Convert.ToInt64((Decimal)rdr.GetValue(colIdx));
            }

            throw new NotImplementedException("Integer fetching/parsing has for " + cxn.getProvider() + " has not been implemented");
        }



        internal static string safeGet(IDataReader rdr, string columnName)
        {
            Int32 colIdx = rdr.GetOrdinal(columnName);
            if (colIdx < 0 || rdr.IsDBNull(colIdx))
            {
                return null;
            }

            return (String)rdr.GetValue(colIdx);
        }

        internal static T safeGet<T>(IDataReader rdr, string columnName)
        {
            Int32 colIdx = rdr.GetOrdinal(columnName);
            if (colIdx < 0 || rdr.IsDBNull(colIdx))
            {
                return default(T);
            }

            return (T)rdr.GetValue(colIdx);
        }

        internal static string safeGet(IDataReader rdr, int columnIndex)
        {
            if (rdr.IsDBNull(columnIndex))
            {
                return null;
            }
            return (String)rdr.GetValue(columnIndex);
        }

        internal static bool readerHasColumn(IDataReader rdr, string columnName, bool ignoreCase = true)
        {
            for (int i = 0; i < rdr.FieldCount; i++)
            {
                if (String.Equals(rdr.GetName(i), columnName, ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture))
                {
                    return true;
                }
            }

            return false;
        }
    }
}