﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Configuration;

namespace com.bitscopic.praediauth.utils
{
    public static class HttpUtils
    {
        /// <summary>
        /// Look for redirect portion of URL based on JWT provider type in config. Return string containing the provider's 
        /// 'redirect' string along with final location. For example: 'https://login.bitscopic.com/login.aspx?return_to=https://bitscopic.zendesk.com/ticket/123'
        /// should produce: 'return_to=https://bitscopic.zendesk.com/ticket/123'
        /// </summary>
        /// <param name="originalRawRequestURL"></param>
        /// <returns></returns>
        public static String getFormattedRedirectURLFromRequest(String originalRawRequestURL)
        {
            if (String.IsNullOrEmpty(originalRawRequestURL) || !originalRawRequestURL.Contains("?"))
            {
                return String.Empty;
            }

            String lowerCaseURL = originalRawRequestURL.ToLower();
            String returnToString = "";
            if (String.Equals("Zendesk", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                String zendeskRedirectString = ConfigurationManager.AppSettings["ZendeskRedirectString"].ToLower(); // return_to
                if (lowerCaseURL.Contains((zendeskRedirectString + "=")))
                {
                    returnToString = originalRawRequestURL.Substring(lowerCaseURL.IndexOf((zendeskRedirectString + "=")));
                }
            }
            else if (String.Equals("Strikedeck", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                returnToString = ConfigurationManager.AppSettings["StrikedeckRedirectString"] + "=https://support.bitscopic.com";

                String sdRedirectString = ConfigurationManager.AppSettings["StrikedeckRedirectString"].ToLower(); // returnTo


                LogUtils.Log("SD redirect from config: " + sdRedirectString + " -- original raw URL: " + originalRawRequestURL);
                if (lowerCaseURL.Contains((sdRedirectString + "=")))
                {
                    returnToString = originalRawRequestURL.Substring(lowerCaseURL.IndexOf((sdRedirectString + "=")));
                }
                else if (lowerCaseURL.Contains("redirectto=http"))
                {
                    returnToString = String.Concat(
                                  ConfigurationManager.AppSettings["StrikedeckRedirectString"],
                                  originalRawRequestURL.Substring(lowerCaseURL.IndexOf("redirectto=") + 10)); // only plus 10 so we include '=' sign
                }
            }

            LogUtils.Log("Fetched redirect from URL: " + returnToString);

            if (!String.IsNullOrEmpty(returnToString))
            {
                if (returnToString.IndexOf("&") >= 0) // look for other query params - if found after 'return to' param, remove them
                {
                    returnToString = returnToString.Substring(0, returnToString.IndexOf("&"));
                }

                return returnToString;
            }

            return String.Empty;
        }

        public static Dictionary<String, String> lastRequestHeaders;

        public static String Post(Uri baseUri, String resource, String postBody)
        {
            if (null == baseUri)
            {
                baseUri = new Uri(ConfigurationManager.AppSettings["CrudSvcBaseUri"]);
            }

            WebRequest request = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "POST";
            request.ContentType = "application/json";

            byte[] requestByteAry = System.Text.Encoding.UTF8.GetBytes(postBody);
            request.ContentLength = requestByteAry.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(requestByteAry, 0, requestByteAry.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                String responseBody = rdr.ReadToEnd();
                return responseBody;
            }
            else
            {
                // TODO - handle error
                throw new WebException(System.Enum.GetName(typeof(HttpWebResponse), response.StatusCode));
            }
        }

        public static String Get(Uri baseUri, String resource, Dictionary<String, String> headers)
        {
            if (null == baseUri)
            {
                baseUri = new Uri(ConfigurationManager.AppSettings["CrudSvcBaseUri"]);
            }

            WebRequest request = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "GET";
            if (headers != null && headers.Count > 0)
            {
                foreach (String key in headers.Keys)
                {
                    request.Headers.Add(key, headers[key]);
                }
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                String responseBody = rdr.ReadToEnd();
                return responseBody;
            }
            else
            {
                // TODO - handle error
                throw new WebException(System.Enum.GetName(typeof(HttpWebResponse), response.StatusCode));
            }
        }

        public static String Get(Uri baseUri, String resource)
        {
            if (null == baseUri)
            {
                baseUri = new Uri(ConfigurationManager.AppSettings["CrudSvcBaseUri"]);
            }

            WebRequest request = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                String responseBody = rdr.ReadToEnd();
                return responseBody;
            }
            else
            {
                // TODO - handle error
                throw new WebException(System.Enum.GetName(typeof(HttpWebResponse), response.StatusCode));
            }
        }

        public static String Put(Uri baseUri, String resource, String putBody)
        {
            if (null == baseUri)
            {
                baseUri = new Uri(ConfigurationManager.AppSettings["CrudSvcBaseUri"]);
            }

            WebRequest request = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "PUT";
            request.ContentType = "application/json";

            byte[] requestByteAry = System.Text.Encoding.UTF8.GetBytes(putBody);
            request.ContentLength = requestByteAry.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(requestByteAry, 0, requestByteAry.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                String responseBody = rdr.ReadToEnd();
                return responseBody;
            }
            else
            {
                // TODO - handle error
                throw new WebException(System.Enum.GetName(typeof(HttpWebResponse), response.StatusCode));
            }

        }

        public static String Delete(Uri baseUri, String resource, String deleteBody)
        {
            if (null == baseUri)
            {
                baseUri = new Uri(ConfigurationManager.AppSettings["CrudSvcBaseUri"]);
            }

            WebRequest request = WebRequest.Create(String.Concat(baseUri, resource));
            request.Method = "DELETE";

            if (!String.IsNullOrEmpty(deleteBody))
            {
                byte[] requestByteAry = System.Text.Encoding.UTF8.GetBytes(deleteBody);
                request.ContentLength = requestByteAry.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(requestByteAry, 0, requestByteAry.Length);
                requestStream.Close();
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            setMyHeadersFromResponse(response);
            Stream stream = response.GetResponseStream();
            StreamReader rdr = new StreamReader(stream);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                String responseBody = rdr.ReadToEnd();
                return responseBody;
            }
            else
            {
                // TODO - handle error
                throw new WebException(System.Enum.GetName(typeof(HttpWebResponse), response.StatusCode));
            }
        }

        internal static string encodeUrl(string rawUrl)
        {
            return System.Web.HttpUtility.UrlEncode(rawUrl);
        }

        internal static string unencodeUrl(string rawUrl)
        {
            return System.Web.HttpUtility.UrlDecode(rawUrl);
        }

        internal static Dictionary<String, String> parseUrl(string rawUrl)
        {
            System.Collections.Specialized.NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(rawUrl);
            Dictionary<String, String> result = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (String nvcKey in nvc.AllKeys)
            {
                if (!result.ContainsKey(nvcKey))
                {
                    result.Add(nvcKey, nvc[nvcKey]);
                }
            }
            return result;
        }

        static void setMyHeadersFromResponse(HttpWebResponse response)
        {
            if (response.Headers != null && response.Headers.Count > 0)
            {
                HttpUtils.lastRequestHeaders = new Dictionary<string, string>();
                foreach (String headerKey in response.Headers.Keys)
                {
                    HttpUtils.lastRequestHeaders.Add(headerKey, response.Headers[headerKey]);
                }
            }
        }
    }
}
