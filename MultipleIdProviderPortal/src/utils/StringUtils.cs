﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.bitscopic.praediauth.utils
{
    public static class StringUtils
    {
        public static String getNewGuid()
        {
            return System.Guid.NewGuid().ToString();
        }

        public static String base64UrlEncode(String s)
        {
            return urlEncodeBase64String(Convert.ToBase64String(Encoding.UTF8.GetBytes(s)));
        }

        public static String urlEncodeBase64String(String s)
        {
            return s.Replace('+', '-').Replace('/', '_').Replace("=", "");
        }

        internal static Dictionary<string, string> toDictFromDelimited(string delimitedString, String recordDelimiter, String keyValDelimiter)
        {
            Dictionary<String, String> result = new Dictionary<string, string>();

            if (String.IsNullOrEmpty(delimitedString))
            {
                return result;
            }

            String[] records = StringUtils.split(delimitedString, new String[] { recordDelimiter }, StringSplitOptions.None);
            for (int i = 0; i < records.Length; i++)
            {
                if (String.IsNullOrEmpty(records[i]) || !records[i].Contains(keyValDelimiter))
                {
                    continue;
                }

                Int32 keyValDelimIdx = records[i].IndexOf(keyValDelimiter);
                String piece1 = records[i].Substring(0, keyValDelimIdx);
                String piece2 = records[i].Substring(keyValDelimIdx + 1);

                result.Add(piece1, piece2);
            }

            return result;
        }

        public static String[] split(String s, String[] delimiters, StringSplitOptions options)
        {
            if (String.IsNullOrEmpty(s) || delimiters == null || delimiters.Length == 0)
            {
                throw new ArgumentNullException("You must supply a non-empty string and delimiter");
            }
            String[] pieces = s.Split(delimiters, options);
            return pieces;
        }

    }
}