﻿using System;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace com.bitscopic.praediauth.utils
{
    public static class SerializerUtils
    {
        /*
        internal static JsonSerializerSettings SERIALIZER_SETTINGS = new JsonSerializerSettings()
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DateTimeZoneHandling = DateTimeZoneHandling.Utc
        };
         * */

        public static JObject deserializeToJObj(String s)
        {
            if (String.IsNullOrEmpty(s))
            {
                s = "{}";
            }
            return Newtonsoft.Json.Linq.JObject.Parse(s);
        }

        public static JArray deserializeToJArray(String s)
        {
            if (String.IsNullOrEmpty(s))
            {
                s = "[]";
            }
            return Newtonsoft.Json.Linq.JArray.Parse(s);
        }

        public static String serializeForPrinting(object arg, bool includeNulls = true)
        {
            if (arg is String)
            {
                return (String)arg;
            }
            if (!includeNulls)
            {
                return JsonConvert.SerializeObject(arg, Formatting.Indented, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }
            return JsonConvert.SerializeObject(arg, Formatting.Indented);
        }

        public static String serialize(object arg, bool includeNulls = true)
        {
            if (arg is String)
            {
                return (String)arg;
            }
            //return JsonConvert.SerializeObject(arg, SerializerUtils.SERIALIZER_SETTINGS);
            if (!includeNulls)
            {
                return JsonConvert.SerializeObject(arg, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }
            return JsonConvert.SerializeObject(arg);
        }

        public static MemoryStream serializeToStream(object arg, bool includeNulls = true)
        {
            String serialized = SerializerUtils.serialize(arg, includeNulls);
            return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(serialized));
        }

        public static T deserialize<T>(String s)
        {
            if (typeof(T) == typeof(String))
            {
                return (T)(object)s;
            }
            return JsonConvert.DeserializeObject<T>(s);
        }

        public static T deserializeFromStream<T>(Stream s)
        {
            using (StreamReader sr = new StreamReader(s))
            {
                string textToDeserialize = sr.ReadToEnd();
                if (typeof(T) == typeof(String))
                {
                    return (T)(object)textToDeserialize;
                }
                return JsonConvert.DeserializeObject<T>(textToDeserialize);
            }
        }

        public static JObject exceptionToJObject(Exception exc)
        {
            JObject jobj = new JObject();
            jobj.Add("Message", exc.Message);
            jobj.Add("HResult", exc.HResult);
            jobj.Add("Inner", exc.InnerException == null ? "<null>" : exc.InnerException.Message);
            jobj.Add("Stack", exc.StackTrace);

            return jobj;
        }

        public static string serializeExceptionSimple(Exception exc, Int32 innerDepth = 1)
        {
            if (innerDepth > 3)
            {
                innerDepth = 3;
            }
            JObject jobj = new JObject();
            jobj.Add("Message", exc.Message);
            jobj.Add("HResult", exc.HResult);
            //jobj.Add("Inner", "<null>");
            if (exc.InnerException != null && innerDepth > 1)
            {
                JObject inner = exceptionToJObject(exc.InnerException);
                jobj.Add("Inner", SerializerUtils.serializeExceptionSimple(exc.InnerException, innerDepth - 1));
            }
            jobj.Add("Stack", exc.StackTrace);

            return jobj.ToString();
        }

        public static bool looksLikeJson(String s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return false;
            }

            if (s.StartsWith("{") && s.EndsWith("}") ||
                s.StartsWith("[") && s.EndsWith("]"))
            {
                return true;
            }

            return false;
        }
    }
}