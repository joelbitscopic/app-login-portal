﻿using com.bitscopic.praediauth.domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;

namespace com.bitscopic.praediauth.utils
{
    public class JWTUtils
    {
        public String getJWT(BitscopicUser user)
        {
            if (String.Equals("Zendesk", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                return getZendeskJWTToken(ConfigurationManager.AppSettings["ZendeskSharedSecret"], user);
            }
            else if (String.Equals("Strikedeck", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                return getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
            }

            throw new ConfigurationErrorsException("The login portal appears to be configured incorrectly - unable to determine JWT token type");
        }

        public String getStrikedeckJWTToken(String key, BitscopicUser user)
        {
            String headerSerialized = SerializerUtils.serialize(new { alg = "HS256", cty = "text/plain" });
            String headerBase64Encoded = StringUtils.base64UrlEncode(headerSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(headerSerialized));

            //String claimSerialized = SerializerUtils.serialize(user, false);
            String claimSerialized = SerializerUtils.serialize(new
                                                                {
                                                                    iss = "bitscopic",
                                                                    iat = Math.Floor(DateTime.UtcNow.Subtract(new TimeSpan(24, 0, 0)).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds),
                                                                    jti = StringUtils.getNewGuid(), // "dbdc62a0-c7c4-4e37-9788-112c8e9371e5",
                                                                    firstName = user.firstName,
                                                                    //lastName = user.lastName,
                                                                    profileImage = "",
                                                                    email = user.email,
                                                                    role = user.role,
                                                                    organization = user.organization,
                                                                    products = user.products
                                                                });
            String claimBase64Encoded = StringUtils.base64UrlEncode(claimSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(claimSerialized));

            String sig = headerBase64Encoded + "." + claimBase64Encoded;

            String encodedSigHash = Convert.ToBase64String(new HMACSHA256(System.Text.Encoding.UTF8.GetBytes(key)).ComputeHash(System.Text.Encoding.UTF8.GetBytes(sig)));
            String urlEncodedSigHash = StringUtils.urlEncodeBase64String(encodedSigHash);
             
            return headerBase64Encoded + "." + claimBase64Encoded + "." + urlEncodedSigHash;

        }

        public String getZendeskJWTToken(String key, BitscopicUser user)
        {
            String headerSerialized = SerializerUtils.serialize(new { alg = "HS256", cty = "text/plain" });
            String headerBase64Encoded = StringUtils.base64UrlEncode(headerSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(headerSerialized));

            //String claimSerialized = SerializerUtils.serialize(user, false);
            String claimSerialized = SerializerUtils.serialize(new
            {
                iat = Math.Floor(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds),
                jti = StringUtils.getNewGuid(), // "dbdc62a0-c7c4-4e37-9788-112c8e9371e5",
                email = user.email, // "joel.mewton@va.gov",
                name = user.firstName + " " + user.lastName, // "Joel M",
               // external_id = "chris@bitscopic.com",
                organization = getOrganization(user)
            });
            String claimBase64Encoded = StringUtils.base64UrlEncode(claimSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(claimSerialized));

            String sig = headerBase64Encoded + "." + claimBase64Encoded;

            String encodedSigHash = Convert.ToBase64String(new HMACSHA256(System.Text.Encoding.UTF8.GetBytes(key)).ComputeHash(System.Text.Encoding.UTF8.GetBytes(sig)));
            String urlEncodedSigHash = StringUtils.urlEncodeBase64String(encodedSigHash);

            return headerBase64Encoded + "." + claimBase64Encoded + "." + urlEncodedSigHash;
        }
        
        String getOrganization(BitscopicUser user)
        {
            if (!String.IsNullOrEmpty(user.organization))
            {
                return user.organization;
            }
            else if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultOrganization"]))
            {
                return ConfigurationManager.AppSettings["DefaultOrganization"];
            }
            else
            {
                return "VA";
            }
        }

        public String getJWTToken(String key, String clientId, IAMOAuth2UserInfo userInfo, BitscopicUser mappedUser = null)
        {
            String headerSerialized = SerializerUtils.serialize(new { alg = "HS256", cty = "text/plain" });
            String headerBase64Encoded = StringUtils.base64UrlEncode(headerSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(headerSerialized));

            Dictionary<string, string> assertedIdentities = new Dictionary<string, string>();
            assertedIdentities.Add("bitscopic", "{}");
            assertedIdentities.Add("vaiam", SerializerUtils.serialize(userInfo, false));
            if (mappedUser != null) // have a mapped user! leaving the bare minimum in the vaiam identity for now - will refactor later and remove completely in favor of the bitscopic identity only
            {
                IAMOAuth2UserInfo newIAMInfo = new IAMOAuth2UserInfo();
                newIAMInfo.fediamsecid = userInfo.fediamsecid;
                assertedIdentities["vaiam"] = SerializerUtils.serialize(newIAMInfo, false);
                assertedIdentities["bitscopic"] = SerializerUtils.serialize(mappedUser, false);
            }

            String claimSerialized = SerializerUtils.serialize(new
            {
                aud = clientId,
                iat = Math.Floor(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds),
                iss = "https://login.bitscopic.va.gov/praediauth",
                jti = StringUtils.getNewGuid(), // "dbdc62a0-c7c4-4e37-9788-112c8e9371e5",
                identity = assertedIdentities,
                //stsToken = userInfo.samlToken,
                //fediamsecid = userInfo.fediamsecid,                
            });
            String claimBase64Encoded = StringUtils.base64UrlEncode(claimSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(claimSerialized));

            String sig = headerBase64Encoded + "." + claimBase64Encoded;

            String encodedSigHash = Convert.ToBase64String(new HMACSHA256(System.Text.Encoding.UTF8.GetBytes(key)).ComputeHash(System.Text.Encoding.UTF8.GetBytes(sig)));
            String urlEncodedSigHash = StringUtils.urlEncodeBase64String(encodedSigHash);

            return headerBase64Encoded + "." + claimBase64Encoded + "." + urlEncodedSigHash;
        }

        public String getBitscopicIdentityJWTToken(String key, String clientId, BitscopicIdentity bitscopicIdentity)
        {
            String headerSerialized = SerializerUtils.serialize(new { alg = "HS256", cty = "text/plain" });
            String headerBase64Encoded = StringUtils.base64UrlEncode(headerSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(headerSerialized));

            String claimJTI = StringUtils.getNewGuid();
            String claimSerialized = SerializerUtils.serialize(new
            {
                aud = clientId,
                iat = Math.Floor(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds),
                iss = "https://login.bitscopic.va.gov/praediauth",
                jti = claimJTI, // "dbdc62a0-c7c4-4e37-9788-112c8e9371e5",
                identity = bitscopicIdentity
                //stsToken = userInfo.samlToken,
                //fediamsecid = userInfo.fediamsecid,                
            });

            String claimBase64Encoded = StringUtils.base64UrlEncode(claimSerialized); // Convert.ToBase64String(Encoding.UTF8.GetBytes(claimSerialized));

            String sig = headerBase64Encoded + "." + claimBase64Encoded;

            String encodedSigHash = Convert.ToBase64String(new HMACSHA256(System.Text.Encoding.UTF8.GetBytes(key)).ComputeHash(System.Text.Encoding.UTF8.GetBytes(sig)));
            String urlEncodedSigHash = StringUtils.urlEncodeBase64String(encodedSigHash);

            String jwt = headerBase64Encoded + "." + claimBase64Encoded + "." + urlEncodedSigHash;
            MyConfigurationManager.setValue(claimJTI, jwt);
            return jwt;
        }

    }
}