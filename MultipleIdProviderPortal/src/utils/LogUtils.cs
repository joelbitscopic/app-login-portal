﻿using System;
using System.Configuration;

namespace com.bitscopic.praediauth.utils
{
    public static class LogUtils
    {
        static String _logPath = 
            String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogFilePath"]) ? 
                @"C:\inetpub\wwwroot\resources\logs\login-portal.log" // default
                : 
                ConfigurationManager.AppSettings["LogFilePath"]; // config file value

        static readonly object _locker = new object();

        public static void Log(String message, bool timestamp = true)
        {
            if (String.IsNullOrEmpty(message))
            {
                return;
            }

            try
            {
                lock (_locker)
                {
                    if (timestamp)
                    {
                        message = String.Concat(DateTime.UtcNow.ToString("o"), "> ", message);
                    }
                    System.IO.File.AppendAllText(_logPath, message + Environment.NewLine);
                }
            }
            catch (Exception) { /* swallow */ }
        }
    }
}