﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace com.bitscopic.praediauth.utils
{
    public static class CryptoUtils
    {
        private static String _salt = "~B1750p1c~";

        public static String hash(String s)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(s + _salt);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            StringBuilder sb = new StringBuilder();
            foreach (byte x in hash)
            {
                sb.Append(x.ToString("x2"));
            }
            return sb.ToString();
        }
    }
}