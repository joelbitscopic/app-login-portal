﻿using com.bitscopic.praediauth.dao;
using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.bitscopic.praediauth
{
    public partial class Login : System.Web.UI.Page
    {
        public static String _session = "MySession";

        protected void Page_Load(object sender, EventArgs e)
        {
            // https://login.bitscopic.com?returnTo=bitscopic.zendesk.com/issue/32
            if (!Page.IsPostBack)
            {
                setRedirectToSessionVar();
            }

            // see is there is a redirect parameter
            if (Page.IsPostBack)
            {
                setRedirectToSessionVar();
                setMessage("");
                return;
            }
        }

        private void setRedirectToSessionVar()
        {
            try
            {
                String[] valsFromUrl = extractUserIdAppNameAndAppApiKeyFromRawUrl(Request.RawUrl);
                if (valsFromUrl != null && valsFromUrl.Length == 4) // we can just blindly call this and pass out of this try/catch if args are not present
                {
                    LogUtils.Log("Detected request args for automatic user login from app launch: " + Request.RawUrl);

                    String userId = valsFromUrl[0]; // Request.Params["app_user_id"];
                    userId = userId.Replace("%40", "@"); // not being URL decoded in all instances... shouldn't be a problem to do this here manually

                    if (!userId.Contains("@va.gov"))
                    {
                        setMessage("App-based logins are currently only enabled for users in the 'va.gov' domain. Unable continue with your login...");
                        return;
                    }

                    String apiClient = valsFromUrl[1]; // Request.Params["app_name"];
                    String apiKey = valsFromUrl[2]; // Request.Params["app_api_key"];
                    String configSettingTag = apiClient + "_api_key"; // e.g. praedico_api_key

                    if (String.IsNullOrEmpty(ConfigurationManager.AppSettings[configSettingTag])
                        || !String.Equals(ConfigurationManager.AppSettings[configSettingTag], apiKey, StringComparison.CurrentCultureIgnoreCase))
                    {
                        LogUtils.Log("Invalid API key or other issue from request. Didn't find app API key setting for " + configSettingTag + 
                            " or didn't match config API key: " + ConfigurationManager.AppSettings[configSettingTag]);
                        setMessage("Invalid API key - unable to automatically login user. Please contact Bitscopic support!");
                        return;
                    }

                    lookupUserAndRedirect(userId, valsFromUrl[3]);
                    return;
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception e)
            {
                LogUtils.Log("Unexpected exception when attempting to authenticate user from URL string: " + Request.RawUrl + " --> " + e.Message);
                setMessage("There was an unexpected error when attempting to authenticate user from URL. Please contact support@bitscopic.com for assistance!");
                return;
            }

            try
            {
                // first try and get from hidden form value since JS has access to unadulterated URL
                Control ctrl = Master.FindControl("hiddenRedirectField");
                if (ctrl != null && ctrl is HiddenField && !String.IsNullOrEmpty(((HiddenField)ctrl).Value))
                {
                    String redirectStr = ((HiddenField)ctrl).Value;
                    Session[_session] = redirectStr;
                    LogUtils.Log("Successfully grabbed redirect from hidden field: " + redirectStr);
                }
                else // if hidden form field check fails then see if raw URL has redirect (even though it *may* not be 100% correct due to IIS)
                {
                    if (Request.Params != null)
                    {
                        // LogUtils.Log("Rw URL: " + Request.RawUrl);
                        String redirectStr = HttpUtils.getFormattedRedirectURLFromRequest(Request.RawUrl); // Request.RawUrl.Substring(Request.RawUrl.IndexOf("redirectTo"));
                        //redirectStr = redirectStr.Substring("redirectTo=".Length); // remove this part of redirect string
                        Session[_session] = redirectStr;
                        LogUtils.Log("Successfully set redirect to: " + redirectStr);
                        // LogUtils.Log("Original URL: " + Request.Url.OriginalString);
                    }
                }
            }
            catch (Exception)
            {
                LogUtils.Log("There was a problem setting the redirect for URL: " + Request.RawUrl);
            }
        }

        protected void click_bitscopic_login(Object sender, EventArgs e)
        {
            try
            {
                String username = bitscopic_login_username.Text;
                String password = bitscopic_login_password.Text;

                if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                {
                    setMessage("Missing username/password");
                    return;
                }

                BitscopicUser dbUser = getBitscopicUserByBitscopicLogin(username, password);
                if (dbUser == null)
                {
                    setMessage("No user with those credentials!");
                    return;
                }
                else // valid user - get token and redirect
                {
                    LogUtils.Log("Successfully authenticated " + dbUser.email + " through Bitscopic login!!");
                    String jwt = new JWTUtils().getJWT(dbUser); //.getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
                    String fullRedirectUrl = getFullRedirectUrl(jwt, Request.RawUrl);
                    LogUtils.Log("Sending user to: " + fullRedirectUrl);
                    Response.Redirect(fullRedirectUrl, true);
                    return;
                }
            }
            catch (Exception exc)
            {
                LogUtils.Log("An error occurred during Bitscopic login: " + exc.Message);
                setMessage("Whoops... something went wrong. Please contact your favorite Bitscopic support person (Joel...) for assistance: " + exc.Message);
            }

        }
        void setMessage(String message)
        {
            labelMessage.Text = message;
        }

        //void setSessionAndRedirect(BitscopicUser user, String redirectToPiece)
        //{
        //    LogUtils.Log("Successfully authenticated " + user.email + " through Bitscopic login!!");
        //    String jwt = new JWTUtils().getJWT(user); //.getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
        //    String fullRedirectUrl = getRedirectUrl(jwt, Request.Url.OriginalString);
        //    LogUtils.Log("Sending user to: " + fullRedirectUrl);
        //    Response.Redirect(fullRedirectUrl, true);
        //}

        String getFullRedirectUrl(String jwt, String originalRequestUrl)
        {
            String returnToPart = HttpUtils.getFormattedRedirectURLFromRequest(originalRequestUrl);
            if (String.Equals("Zendesk", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                return ConfigurationManager.AppSettings["BaseJWTURL"] + jwt + (String.IsNullOrEmpty(returnToPart) ? "" : returnToPart);
            }
            else if (String.Equals("Strikedeck", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
            {
                return ConfigurationManager.AppSettings["BaseJWTURL"] + jwt + (String.IsNullOrEmpty(returnToPart) ? "&returnTo=https://support.bitscopic.com" : returnToPart);
            }
            else
            {
                LogUtils.Log("Unable to redirect to help desk - the portal's configuration is incomplete. Please contact Bitscopic");
                setMessage("Unable to redirect to help desk - the portal's configuration is incomplete. Please contact Bitscopic");
                Response.End();
                return null;
            }
        }

        BitscopicUser getBitscopicUserByBitscopicLogin(String username, String password)
        {
            using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
            {
                cxn.Open();
                SqliteUserDao userDao = new SqliteUserDao(cxn);
                return userDao.getUserByIdAndPassword(username, password);
            }
        }

        String[] extractUserIdAppNameAndAppApiKeyFromRawUrl(String rawUrl)
        {
            List<String> result = new List<string>();

            rawUrl = HttpUtils.unencodeUrl(rawUrl);

            if (String.IsNullOrEmpty(rawUrl)
                || !rawUrl.ToLower().Contains("app_user_id")
                || !rawUrl.ToLower().Contains("app_name")
                || !rawUrl.ToLower().Contains("app_api_key"))
            {
                return null;
            }

            //Dictionary<String, String> urlParams = HttpUtils.parseUrl(rawUrl);
            //if (urlParams.ContainsKey("app_user_id") && urlParams.ContainsKey("app_name") && urlParams.ContainsKey("app_api_key")) // whoop! this helper was able to extract args from url
            //{
            //    result.Add(urlParams["app_user_id"]);
            //    result.Add(urlParams["app_name"]);
            //    result.Add(urlParams["app_api_key"]);
            //}
            //else // unable to build nice dictionary with utility but we still know the URL contains the user info we need so must parse manually!
            //{
            LogUtils.Log("Manually parsing request URL: " + rawUrl);
            Dictionary<String, String> manuallyParsedDict = StringUtils.toDictFromDelimited(rawUrl.Replace("?", "&"), "&", "=");
            LogUtils.Log("URL params dict: " + SerializerUtils.serializeForPrinting(manuallyParsedDict));
            result.Add(manuallyParsedDict["app_user_id"]);
            result.Add(manuallyParsedDict["app_name"]);
            result.Add(manuallyParsedDict["app_api_key"]);
            result.Add(manuallyParsedDict[ConfigurationManager.AppSettings["ZendeskRedirectString"]]);
            //}
            return result.ToArray();
        }

        void lookupUserAndRedirect(String userId, String redirectUrl)
        {
            try
            {
                BitscopicUser userAccount = null;
                using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
                {
                    cxn.Open();
                    SqliteUserDao userDao = new SqliteUserDao(cxn);
                    userAccount = userDao.getUserByIdAndIdentityProvider(userId, "PIV");
                }

                if (userAccount == null)
                {
                    setMessage(String.Format(
                        "We didn't find your ID in our system. Click <a href=\"mailto:{0}?subject=Bitscopic%20Support%20Login%20Portal%20Access%20Request:%20{1}\">HERE</a> " +
                        "to request access (will open your default mail client)",
                        ConfigurationManager.AppSettings["SupportEmail"],
                        userId));
                    return;
                }

                // found active user -> successful login - create our session!
                LogUtils.Log("Found " + userId + " in user table!");

                String fullRedirectUrl = ConfigurationManager.AppSettings["BaseJWTURL"] + new JWTUtils().getJWT(userAccount) + (String.IsNullOrEmpty(redirectUrl) ? "" : "&return_to=" + redirectUrl);

                //String fullRedirectUrl = getFullRedirectUrl(new JWTUtils().getJWT(userAccount), redirectUrl);
                LogUtils.Log("Sending user to: " + fullRedirectUrl);
                Response.Redirect(fullRedirectUrl, true);

                return;
            }
            catch (System.Threading.ThreadAbortException) { /* from response.redirect - just swallow it */ }
            catch (Exception e)
            {
                LogUtils.Log("Oops... something went wrong looking up user from URL request: " + e.Message);
                setMessage("It looks like something went wrong... Sorry about that! Please contact support@bitscopic.com for technical assistance with the Help Desk login portal");
            }
        }

    }
}