﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuthService.aspx.cs" Inherits="com.bitscopic.praediauth.AuthService" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron" style="width: 800px; margin: 0 auto;">

        <div style="width: 40%; margin-left: auto; margin-right: auto; padding-bottom: 40px;">
            <table>
                <tr>
                    <td>
                        <img src="Content/bitscopic-logo.png" alt="Bitscopic" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">Login Portal Auth Service - See https://bitscopic.atlassian.net/wiki/spaces/AUTH/overview for details</td>
                </tr>
            </table>

            
        </div>


        <hr style="border: 1px solid #6d6d6d; margin-bottom: 40px; margin-top: 40px;" />

        <div id="div_bitscopic_user_login" style="width: 50%; margin-left: auto; margin-right: auto;">
            <table id="table-bitscopic-user-login">
                <tr style="padding-top: 20px;">
                    <td colspan="2" style="text-align: center"><asp:Label ID="labelMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label></td>
                </tr>
            </table>            
        </div>
    </div>

</asp:Content>
