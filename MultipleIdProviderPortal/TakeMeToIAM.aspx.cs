﻿using com.bitscopic.praediauth.utils;
using System;
using System.Configuration;

namespace com.bitscopic.praediauth
{
    public partial class TakeMeToIAM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // fOMB2pxvphpSp7AVVypl
            // Bitscopic_NP
            // https://vhapalapppg02.v21.med.va.gov/DEV_login_portal/iam_sso_login
            // https://int.fed.eauth.va.gov/oauthi/sps/oauth/oauth20/authorize?client_id=%3cclientId%3e&response_type=code&aud=rsrServer1%20rsrServer2&scope=openid%20fhirUser&state=1L&redirect_uri=%3cregistered_redirect_uri

            String iamURL = "https://ssoi.idev.iam.va.gov/va-single-sign-on?redirect_uri=";
            String redirectURL = "https://vhapalapppg02.v21.med.va.gov/login_portal/iam_sso_login.aspx";
        //    String formattedURL = "client_id={0}&response_type=code&redirect_uri={1}&scope={2}&state={3}";
        //    String scopeStr = "openid%20fhirUser";
            

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IAM_URL"]))
            {
                iamURL = ConfigurationManager.AppSettings["IAM_URL"];
            }
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IAM_REDIRECT_URL"]))
            {
                redirectURL = ConfigurationManager.AppSettings["IAM_REDIRECT_URL"];
            }

            try
            {
                LogUtils.Log("Redirecting to: " + iamURL + HttpUtils.encodeUrl(redirectURL));
                Response.Redirect(iamURL + HttpUtils.encodeUrl(redirectURL), true);
                return;
            }
            catch (Exception)
            {
                //swallow thread abort
            }
        }
    }
}