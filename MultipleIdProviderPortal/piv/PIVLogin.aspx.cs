﻿using com.bitscopic.praediauth.dao;
using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.bitscopic.praediauth
{
    public partial class PIVLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                setNotification("");
            }
            //try // see if this hidden field has been set for a proper redirect - have to do this stupid nonsense becase IIS is filtering '#' character and Shabd isn't encoding redirect URL...
            //{
            //    if (!String.IsNullOrEmpty(Request.Cookies["redirectTo"].Value))
            //    {
            //        String redirectStr = Request.Cookies["redirectTo"].Value;
            //        Session[Login._session] = redirectStr;
            //        LogUtils.Log("Successfully grabbed redirect from cookie for PIV login: " + redirectStr);
            //    }
            //}
            //catch (Exception) { }


            if (Request.ClientCertificate == null || !Request.ClientCertificate.IsPresent)
            {
                //List<BitscopicUser> userAccounts = lookupPIVUser("joel.mewton@va.gov", "");
                //setSessionAndRedirect(userAccounts[0], "");

                setNotification("No PIV card detected. Click <a href=\"https://login.bitscopic.com/loginportal/login.aspx\">HERE</a> to return to login page.");
                return;
            }

            else
            {
                try // see if this hidden field has been set for a proper redirect - have to do this stupid nonsense becase IIS is filtering '#' character and Shabd isn't encoding redirect URL...
                {
                    Control ctrl = Master.FindControl("hiddenRedirectField");
                    if (ctrl != null && ctrl is HiddenField && !String.IsNullOrEmpty(((HiddenField)ctrl).Value))
                    {
                        String redirectStr = ((HiddenField)ctrl).Value;
                        Session[Login._session] = redirectStr;
                        LogUtils.Log("Successfully grabbed redirect from hidden field in PIV login: " + redirectStr);
                    }

                }
                catch (Exception) { }

                try
                {
                    LogUtils.Log("Serialized client cert: " + "\r\n"
                        + "CertEncoding: " + Request.ClientCertificate.CertEncoding.ToString() + "\r\n"
                        + "Cookie: " + Request.ClientCertificate.Cookie + "\r\n"
                        + "Flags: " + Request.ClientCertificate.Flags.ToString() + "\r\n"
                        + "Issuer: " + Request.ClientCertificate.Issuer + "\r\n"
                        + "SerialNumber: " + Request.ClientCertificate.SerialNumber + "\r\n"
                        + "ServerIssuer: " + Request.ClientCertificate.ServerIssuer + "\r\n"
                        + "ServerSubject: " + Request.ClientCertificate.ServerSubject + "\r\n"
                        + "Subject: " + Request.ClientCertificate.Subject + "\r\n"
                        + "ValidFrom: " + Request.ClientCertificate.ValidFrom.ToString() + "\r\n"
                        + "ValidUntil: " + Request.ClientCertificate.ValidUntil.ToString() + "\r\n"
                        + "AllKeys" + SerializerUtils.serialize(Request.ClientCertificate.AllKeys) + "\r\n"
                        );
                    //foreach (String key in Request.ClientCertificate.AllKeys)
                    //{
                    //    LogUtils.Log("Client cert key props => " + key + " : " + Request.ClientCertificate[key]);
                    //}

                    String issuer = Request.ClientCertificate.Issuer; //Veterans Affairs User CA B1
                    String serialNumber = Request.ClientCertificate.SerialNumber; // 61-86-84
                    String subject = Request.ClientCertificate.Subject; //DC=gov, DC=va, O=internal, OU=people, OID.0.9.2342.19200300.100.1.1=joel.mewton@va.gov, CN=Joel A. Mewton 419235 (affiliate)
                    String certUserId = findEmailInSubject(subject);
                    if (String.IsNullOrEmpty(certUserId))
                    {
                        certUserId = findIdInSubject(subject);
                    }

                    if (String.IsNullOrEmpty(certUserId))
                    {
                        setNotification("No email or user ID found in client cert. Please contact support.");
                        // TODO - log everything from cert
                        return;
                    }

                    // get user from DB - note we will need a way to change apps in the future!
                    BitscopicUser userAccount = lookupPIVUser(certUserId, "");

                    if (userAccount == null)
                    {
                        if (false == certUserId.Contains("@") && certUserId.Length > 10) // if not looking up user by email and full cert user ID lookup failed, try using first 10 chars only which is what other apps use
                        {
                            LogUtils.Log("Didn't find user with full identifier - trying to lookup user by first 10 chars: " + certUserId.Substring(0, 10));
                            userAccount = lookupPIVUser(certUserId.Substring(0, 10), "");
                        }

                        if (userAccount == null) // check if still didn't find user
                        {
                            setNotification(String.Format(
                                "We didn't find your ID in our system. Click <a href=\"mailto:{0}?subject=Bitscopic%20Support%20Login%20Portal%20Access%20Request:%20{1}\">HERE</a> " +
                                "to request access (will open your default mail client)",
                                ConfigurationManager.AppSettings["SupportEmail"],
                                certUserId));
                            return;
                        }
                    }

                    // found active user -> successful login - create our session!
                    setNotification("Successful PIV login for " + certUserId);
                    logAccess(userAccount.id, "PIV");

                    string redirectUrl = "";
                    if (!String.IsNullOrEmpty(Session[Login._session] as String))
                    {
                        redirectUrl = Session[Login._session] as String;
                        LogUtils.Log("Found redirect URL for PIV login: " + redirectUrl);
                    }

                    if (String.IsNullOrEmpty(redirectUrl))
                    {
                        if (String.Equals("Strikedeck", ConfigurationManager.AppSettings["JWTType"], StringComparison.CurrentCultureIgnoreCase))
                        {
                            redirectUrl = "returnTo=https://support.bitscopic.com";
                        }
                    }

                    LogUtils.Log("Successfully authenticated PIV ID: " + certUserId + " -- Email: " + userAccount.email + " -- with PIV!!");
                    String jwt = new JWTUtils().getJWT(userAccount); //.getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
                    String fullRedirectUrl = ConfigurationManager.AppSettings["BaseJWTURL"] + jwt + (String.IsNullOrEmpty(redirectUrl) ? "" : ("&" + redirectUrl));
                    LogUtils.Log("Sending user to: " + fullRedirectUrl);
                    Response.Redirect(fullRedirectUrl, true);

                    return;
                }
                catch (System.Threading.ThreadAbortException)
                {
                    // do nothing
                }
                catch (Exception exc)
                {
                    setNotification("PIV authentication failed: " + exc.Message);
                    LogUtils.Log("PIV FAILURE: " + SerializerUtils.serializeForPrinting(exc));
                }
            }
        }

        BitscopicUser lookupPIVUser(String pivId, String appName)
        {
            using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
            {
                cxn.Open();
                SqliteUserDao userDao = new SqliteUserDao(cxn);
                return userDao.getUserByIdAndIdentityProvider(pivId, "PIV");
            }
        }

        String findEmailInSubject(String certSubjectString)
        {
            IList<String> subjectPieces = new List<String>(certSubjectString.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            foreach (String p in subjectPieces)
            {
                if (!(p.Contains("@")))
                {
                    continue;
                }
                String[] keyAndVal = p.Split(new char[] { '=' });
                if (isEmail(keyAndVal[1]))
                {
                    return keyAndVal[1];
                }
            }

            return null;
        }

        String findIdInSubject(String certSubjectString)
        {
            IList<String> subjectPieces = new List<String>(certSubjectString.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            foreach (String p in subjectPieces)
            {
                if (!String.IsNullOrEmpty(p) && (p.Trim().ToUpper().StartsWith("OID.") || p.Trim().ToUpper().StartsWith("ID.")))
                {
                    String[] keyAndVal = p.Split(new char[] { '=' });
                    return keyAndVal[1].Trim();
                }
            }

            return null;
        }

        bool isEmail(String s)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(s);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        void setNotification(String message)
        {
            literalPivLoginMessage.Text = message;
//            ((Label)Page.Master.FindControl("labelNotifications")).Text = message;
        }

        void logAccess(String userId, String accessType)
        {
            try
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogLogins"]) || String.Equals(ConfigurationManager.AppSettings["LogLogins"], "true", StringComparison.CurrentCultureIgnoreCase))
                {
                   // LogUtils.Log(userId + " successfully logged in via " + accessType);
                }
            }
            catch (Exception sqlExc)
            {
            }
        }

    }
}