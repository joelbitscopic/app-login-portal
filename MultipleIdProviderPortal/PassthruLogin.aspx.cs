﻿using com.bitscopic.praediauth.dao;
using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.bitscopic.praediauth
{
    public partial class PassthruPIVLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                setNotification("");
            }

            String clientIP = Request.UserHostAddress;
            if (!isWhitelistedIP(clientIP))
            {
                setNotification("This authentication mechanism only works from VA's network. Please contact Bitscopic support with any questions or concerns.");
                LogUtils.Log("Passthru login was called from NON whitelisted IP: " + clientIP);
                return;
            }
            else
            {
                LogUtils.Log("Passthru login was called from a whitelisted IP: " + clientIP);
            }


            try // see if this hidden field has been set for a proper redirect - have to do this stupid nonsense becase IIS is filtering '#' character and Shabd isn't encoding redirect URL...
            {
                Control ctrl = Master.FindControl("hiddenRedirectField");
                if (ctrl != null && ctrl is HiddenField && !String.IsNullOrEmpty(((HiddenField)ctrl).Value))
                {
                    String redirectStr = ((HiddenField)ctrl).Value;
                    Session[Login._session] = redirectStr;
                    LogUtils.Log("Successfully grabbed redirect from hidden field in passthru login: " + redirectStr);
                }

            }
            catch (Exception) { }

            try
            {
                LogUtils.Log("Passthru 'login'... temporarily allowing access via this method. Using special purpose account");
                // get user from DB - note we will need a way to change apps in the future!
                BitscopicUser userAccount = lookupPIVUser("support.user@bitscopic.com");

                if (userAccount == null)
                {
                    setNotification(String.Format(
                        "An unexpected error has occurred... Please <a href=\"mailto:{0}?subject=Bitscopic%20Support%20Passthru%20Login%20Error\">contact support</a> ",
                        ConfigurationManager.AppSettings["SupportEmail"]
                        ));
                    return;
                }

                string redirectUrl = "";
                if (!String.IsNullOrEmpty(Session[Login._session] as String))
                {
                    redirectUrl = Session[Login._session] as String;
                    LogUtils.Log("Found redirect URL for PIV login: " + redirectUrl);
                }
                else
                {
                    redirectUrl = "https://bitscopic.zendesk.com";
                    LogUtils.Log("Found redirect URL for PIV login: " + redirectUrl);
                }

                LogUtils.Log("Found support.user@bitscopic.com - temporarily using this account for support site access due to PIV issues");
                String jwt = new JWTUtils().getJWT(userAccount); //.getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user);
                String fullRedirectUrl = ConfigurationManager.AppSettings["BaseJWTURL"] + jwt + (String.IsNullOrEmpty(redirectUrl) ? "" : ("&" + redirectUrl));
                LogUtils.Log("Sending user to: " + fullRedirectUrl);
                Response.Redirect(fullRedirectUrl, true);

                return;
            }
            catch (System.Threading.ThreadAbortException)
            {
                // do nothing
            }
            catch (Exception exc)
            {
                setNotification("Sorry... Passthru authentication failed: " + exc.Message);
                LogUtils.Log("PASSTHRU LOGIN FAILURE: " + SerializerUtils.serializeForPrinting(exc));
            }
        }

        private bool isWhitelistedIP(string clientIP)
        {
            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["PassthruLoginWhitelistedIPs"])) // if this isn't set, allow all!
            {
                LogUtils.Log("No IP whitelist in config. Allowing " + clientIP);
                return true;
            }

            if ((new List<String>() { "152.131.10.128", "152.132.15.18" }).Contains(clientIP))
            {
                return true;
            }
            if (clientIP.StartsWith("152.131.10.") || clientIP.StartsWith("152.132.15.") || clientIP.StartsWith("152.132.10."))
            {
                return true;
            }
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PassthruLoginWhitelistedIPs"]))
            {
                List<String> whitelistedIPs = SerializerUtils.deserialize<List<String>>(ConfigurationManager.AppSettings["PassthruLoginWhitelistedIPs"]);
                LogUtils.Log("Found whitelisted IPs in config: " + ConfigurationManager.AppSettings["PassthruLoginWhitelistedIPs"]);
                if (whitelistedIPs.Contains(clientIP))
                {
                    return true;
                }
            }
            return false;
        }

        BitscopicUser lookupPIVUser(String pivId)
        {
            using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
            {
                cxn.Open();
                SqliteUserDao userDao = new SqliteUserDao(cxn);
                return userDao.getUserByIdAndIdentityProvider(pivId, "PIV");
            }
        }

        void setNotification(String message)
        {
            literalPivLoginMessage.Text = message;
//            ((Label)Page.Master.FindControl("labelNotifications")).Text = message;
        }


    }
}