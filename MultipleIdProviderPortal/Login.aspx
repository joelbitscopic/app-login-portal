﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="com.bitscopic.praediauth.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron" style="width: 800px; margin: 0 auto;">

        <div style="width: 40%; margin-left: auto; margin-right: auto; padding-bottom: 40px;">
            <table>
                <tr>
                    <td>
                        <img src="Content/bitscopic-logo.png" alt="Bitscopic" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">Login Portal</td>
                </tr>
            </table>

            
        </div>

        <div id="div-login-buttons">
            <table id="table-login-buttons">
                <tr>
                    <td>
                        <!-- HTML for login w/ PIV -->
                        <div id="bitscopic_piv_login">
<%--                            <a href="piv/pivlogin.aspx" class="bitscopic_piv_button" onclick="">--%>
                            <a href="PassthruLogin.aspx" class="bitscopic_piv_button" onclick="">
                                <img src="Content/veterans-administration_scaled.png" alt="VA"/>&nbsp;&nbsp;&nbsp;Sign in with PIV
                            </a>
                        </div>
                    </td>
                    <td>or</td>
                    <td>
                            <!-- HTML for render Google Sign-In button -->
                            <div id="gSignIn"></div>
                    </td>
                    <td>or</td>
                    <td>
                        <div id="iam_auth">
                            <a href="TakeMeToIAM.aspx" class="iam_sso_button" onclick="">
                                <img src="Content/veterans-administration_scaled.png" alt="IAM"/>&nbsp;&nbsp;&nbsp;IAM SSO
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <hr style="border: 1px solid #6d6d6d; margin-bottom: 40px; margin-top: 40px;" />

        <div id="div_bitscopic_user_login" style="width: 50%; margin-left: auto; margin-right: auto;">
            <table id="table-bitscopic-user-login">
                <tr>
                    <td style="color:orangered; font-size: smaller; text-align: center; padding-bottom: 20px" colspan="2">
                        Login With Your Bitscopic Credentials          
                    </td>
                </tr>
                <tr>
                    <td>Username:</td>
                    <td>
                        <asp:TextBox ID="bitscopic_login_username" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>
                        <asp:TextBox ID="bitscopic_login_password" runat="server" TextMode="Password" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp</td>
                    <td>
                        <asp:Button CssClass="bitscopic_button" Text="Login" runat="server" OnClick="click_bitscopic_login" />
                    </td>
                </tr>
                <tr style="padding-top: 20px;">
                    <td colspan="2" style="text-align: center"><asp:Label ID="labelMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label></td>
                </tr>
            </table>            
        </div>
    </div>

</asp:Content>
