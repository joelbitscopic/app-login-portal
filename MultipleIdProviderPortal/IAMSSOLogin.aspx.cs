﻿using com.bitscopic.praediauth.domain;
using com.bitscopic.praediauth.domain.iam;
using com.bitscopic.praediauth.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using com.bitscopic.praediauth.dao.sql;
using com.bitscopic.praediauth.domain.exception;

namespace com.bitscopic.praediauth
{
    public partial class IAMSSOLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LogUtils.Log("Received request: " + Request.RawUrl);
            //LogUtils.Log("Executing in .NET runtime: " + Environment.Version.ToString());
            // where did request come from? IAM redirect? or was user sent here from our apps? or navigated here directly?
            
            // for IAM integration development, we can consider any request which didn't come from IAM as a redirect as an authentication request
            if (String.IsNullOrEmpty(Request.Params.Get("state")) || null == retrieveState(Request.Params.Get("state")))
            {
                literal_pageContent.Text = "<p>Bad request. Missing or unrecognized state</p>";
                return;
            }

            IDPState retrievedIDPState = SerializerUtils.deserialize<IDPState>(retrieveState(Request.Params.Get("state")));

            if (String.IsNullOrEmpty(retrievedIDPState.client_state) || null == retrieveState(retrievedIDPState.client_state))
            {
                literal_pageContent.Text = "<p>Bad request. Invalid or unrecognized client state</p>";
                return;
            }

            ClientState retrievedClientState = SerializerUtils.deserialize<ClientState>(retrieveState(retrievedIDPState.client_state));

            bool fetchSTS = false;
            if (retrievedClientState.flags != null && retrievedClientState.flags.ContainsKey("sts") && String.Equals(retrievedClientState.flags["sts"], "true", StringComparison.CurrentCultureIgnoreCase))
            {
                fetchSTS = true;
            }


            // authentication request
            String authToken = "";
            String stateId = "";
            authToken = Request.QueryString["code"];
            stateId = Request.QueryString["state"];
            LogUtils.Log("Detected code and state: " + authToken + " / " + stateId);
            String url = "https://int.fed.eauth.va.gov/oauthi/sps/oauth/oauth20/token";
            if (!String.IsNullOrEmpty(MyConfigurationManager.getValue("IAM_OAUTH_TOKEN_URL")))
            {
                url = ConfigurationManager.AppSettings["IAM_OAUTH_TOKEN_URL"];
            }

            String clientId = "Bitscopic_NP";
            String clientSharedSecret = "fOMB2pxvphpSp7AVVypl";
            String redirectUri = "https://vhapalapppg02.v21.med.va.gov/DEV_login_portal/iam_sso_login";
            if (false == String.IsNullOrEmpty(MyConfigurationManager.getValue("AUTH_SERVICE_CLIENT_ID")))
            {
                clientId = MyConfigurationManager.getValue("AUTH_SERVICE_CLIENT_ID");
            }
            if (false == String.IsNullOrEmpty(MyConfigurationManager.getValue("AUTH_SERVICE_CLIENT_SECRET")))
            {
                clientSharedSecret = MyConfigurationManager.getValue("AUTH_SERVICE_CLIENT_SECRET");
            }
            if (false == String.IsNullOrEmpty(ConfigurationManager.AppSettings["IAM_REDIRECT_URL"]))
            {
                redirectUri = ConfigurationManager.AppSettings["IAM_REDIRECT_URL"]; // SECOND: add redirect URL
            }
            var dict = new Dictionary<string, string>();
            dict.Add("client_id", clientId);
            dict.Add("client_secret", clientSharedSecret);
            dict.Add("grant_type", "authorization_code");
            dict.Add("redirect_uri", redirectUri);
            dict.Add("code", authToken);

            LogUtils.Log("Sending request for token to: " + url);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.Expect100Continue = true;
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };
            Task<HttpResponseMessage> httpResponseMessageTask = client.SendAsync(req);
            IAMOAuth2UserInfo userInfo = new IAMOAuth2UserInfo();
            if (httpResponseMessageTask.Wait(30 * 1000))
            {
                Task<String> responseTask = httpResponseMessageTask.Result.Content.ReadAsStringAsync();
                if (false == responseTask.Wait(30 * 1000))
                {
                    LogUtils.Log("Took more than 30 seconds to read response from IAM!! This should never happen...");
                    literal_pageContent.Text += "<p>Took more than 30 seconds to read response from IAM!! This should never happen... Please wait a short time and try again -or- contact support</p>";
                    return;
                }

                String result = responseTask.Result;
                literal_pageContent.Text = "<p>Received a response:</p></p>" + result + "</p>";

                try
                {
                    IAMOAuth2AccessToken resultsDeserialized = SerializerUtils.deserialize<IAMOAuth2AccessToken>(result);
                    userInfo.accessToken = new IAMOAuth2AccessToken() { access_token = resultsDeserialized.access_token, expires_in = resultsDeserialized.expires_in, refresh_token = resultsDeserialized.refresh_token };
                    String rawJwt = resultsDeserialized.id_token;
                    JwtSecurityToken jwt = new JwtSecurityToken(rawJwt);
                    LogUtils.Log("Raw id_token from IAM: " + rawJwt);
                    //userInfo = SerializerUtils.deserialize<IAMOAuth2UserInfo>(jwt..RawPayload);

                    userInfo.fediamsecid = safeGetClaimValue("fediamsecid", jwt.Claims);
                    userInfo.email = safeGetClaimValue("email", jwt.Claims);
                    userInfo.iat = safeGetClaimValue("iat", jwt.Claims);
                    userInfo.iss = safeGetClaimValue("iss", jwt.Claims);
                    //userInfo.sub = safeGetClaimValue("sub", jwt.Claims);
                    userInfo.family_name = safeGetClaimValue("family_name", jwt.Claims);
                    userInfo.given_name = safeGetClaimValue("given_name", jwt.Claims);
                    userInfo.fediamadDomain = safeGetClaimValue("fediamadDomain", jwt.Claims);
                    userInfo.fediamSamAccountName = safeGetClaimValue("fediamadSamAccountName", jwt.Claims);
                    userInfo.fediamadUPN = safeGetClaimValue("fediamadUPN", jwt.Claims);

                    if (false == String.IsNullOrEmpty(userInfo.email)) { userInfo.email = userInfo.email.ToLower(); }
                    if (false == String.IsNullOrEmpty(userInfo.family_name)) { userInfo.family_name = userInfo.family_name.ToLower(); }
                    if (false == String.IsNullOrEmpty(userInfo.given_name)) { userInfo.given_name = userInfo.given_name.ToLower(); }

                    /*
                    JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();
                    var validations = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = false,
                        //   IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                    jwtHandler.ValidateToken(result, validations, out jwt);
                    */

                    StringBuilder jwtClaimsSB = new StringBuilder();
                    jwtClaimsSB.AppendLine(String.Format("Request received: {0} {1}", Request.HttpMethod, Request.Path));
                    foreach (System.Security.Claims.Claim claim in jwt.Claims)
                    {
                        jwtClaimsSB.AppendLine(String.Format("{0}:{1}", claim.Type, claim.Value));
                    }
                    //literal_pageContent.Text += "<p>" + jwtClaimsSB.ToString() + "</p>";

                    // fetch STS
                    if (fetchSTS)
                    {
                        String rawSTSResponse = new SecureTokenService().exchangeOAuthTokenForSAML(resultsDeserialized.access_token);
                        //userInfo.samlToken = rawSTSResponse;
                        //literal_pageContent.Text += "<p>" + rawSTSResponse + "</p>";

                        // set SAML token & update client state identities then put back in 
                        userInfo.setSAMLToken(rawSTSResponse);
                        //retrievedClientState.assertedIdentities = new Dictionary<string, string>();
                        //retrievedClientState.assertedIdentities.Add("vaiam", SerializerUtils.serialize(userInfo));
                        MyConfigurationManager.setValue(retrievedClientState.clientId + "|" + stateId, rawSTSResponse); // e.g.: PG|1L

                        LogUtils.Log("Retrieved STS SAML token for " + userInfo.fediamsecid + "\r\n\r\n========START========\r\n" + rawSTSResponse + "\r\n========END========\r\n\r\n");
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.Log("Error fetching token/STS from IAM: " + SerializerUtils.serializeExceptionSimple(exc));
                    literal_pageContent.Text += "<p>Error parsing token: " + SerializerUtils.serializeExceptionSimple(exc, 2) + "</p>";
                    return;
                }

                    //return;
                
            }
            else
            {
                literal_pageContent.Text = "<p>Hmmm... didn't receive a response to token request...</p>";
                return;
            }

            LogUtils.Log("Selected JWT attributes from IAM: " + SerializerUtils.serialize(userInfo));

            AuthServiceClient authServiceClient = getAuthServiceClient(retrievedClientState.clientId);

            // first check to see if user is in DB using sec id
            BitscopicUser userFromDb = null;
            //bool shouldUpdateDb = false;
            bool lazyCreateUser = true;
            Identity accessedIdentity = null;
            try
            {
                LogUtils.Log("Trying to look up user in DB with fediamsecid...");
                userFromDb = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection()).getUser(userInfo.fediamsecid);
                LogUtils.Log("Found user via fediamsecid: " + userInfo.fediamsecid + " - db key: " + userFromDb.dbKey);
            }
            catch (UserNotFoundException)
            {
                LogUtils.Log("Didn't find user for fediamsecid " + userInfo.fediamsecid + " - trying lookup via alternate identity/'new' sec id...");
                try
                {
                    // next try searching for VA_IAM identity... user may have Bitscopic ID <> fediamsecid!!
                    userFromDb = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection()).getUserByIdentity(IdentityProvider.VA_IAM, userInfo.fediamsecid);
                    try
                    {
                        accessedIdentity = userFromDb.identities.First(id => id.provider == IdentityProvider.VA_IAM && id.id == userInfo.fediamsecid);
                    }
                    catch (Exception) { }
                    LogUtils.Log("Found user via fediamsecid identity: " + userInfo.fediamsecid + " - db key: " + userFromDb.dbKey);
                }
                catch (UserNotFoundException)
                {
                    // last try... look for user by email...
                    if (false == String.IsNullOrEmpty(userInfo.email))
                    {
                        try
                        {
                            LogUtils.Log("Didn't find user for fediamsecid " + userInfo.fediamsecid + " as id or alternate identity... trying lookup via email: " + userInfo.email);
                            userFromDb = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection()).getUserByIdentity(IdentityProvider.VA_EMAIL, userInfo.email);
                            LogUtils.Log("Found user via email idenity: " + userInfo.email + " - db key: " + userFromDb.dbKey + "... adding 'new' fediamsecid from IAM identity assertion to user identities...");
                            //shouldUpdateDb = true; // found user by email! that means user ID and VA_IAM identity have changed... add the new VA_IAM identity for the user
                            Identity newId = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection())
                                .addIdentity(userFromDb.dbKey, new Identity() { provider = IdentityProvider.VA_IAM, id = userInfo.fediamsecid });
                            userFromDb.identities.Add(newId);
                            try
                            {
                                accessedIdentity = userFromDb.identities.First(id => id.provider == IdentityProvider.VA_EMAIL && id.id == userInfo.email);
                            }
                            catch (Exception) { }
                            LogUtils.Log("Successfully added new feediamsecid identity " + userInfo.fediamsecid + " to user with db key " + userFromDb.dbKey);
                        }
                        catch (UserNotFoundException) { /* final try... handle null user below*/ }

                    }
                }
            }

            if (userFromDb == null && lazyCreateUser)
            {
                BitscopicUser newUser = new BitscopicUser() { identities = new List<Identity>() };
                newUser.identities.Add(new Identity() { provider = IdentityProvider.VA_IAM, id = userInfo.fediamsecid });
                if (false == String.IsNullOrEmpty(userInfo.email))
                {
                    newUser.identities.Add(new Identity() { provider = IdentityProvider.VA_EMAIL, id = userInfo.email });
                    newUser.email = userInfo.email;
                }
                newUser.id = userInfo.fediamsecid;
                newUser.firstName = userInfo.given_name;
                newUser.lastName = userInfo.family_name;
                userFromDb = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection()).createBitscopicUser(newUser);
                LogUtils.Log("Lazily created new user: " + SerializerUtils.serialize(newUser, false));
            }

            if (userFromDb == null)
            {
                literal_pageContent.Text = "Unexpected problem with user... please contact support!";
                return;
            }

            if (accessedIdentity != null) // log if we accessed user by identity
            {
                try
                {
                    SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnection()).logAccess(retrievedClientState.clientId, accessedIdentity.dbKey);
                }
                catch (Exception) { /*swallow*/}
            }


            BitscopicIdentity bitscopicId = new BitscopicIdentity();
            bitscopicId.bitscopic_id = userFromDb.id; // userInfo.fediamsecid; // TODO - get this from a DB with logic to match multiple user identities!
            bitscopicId.family_name = userFromDb.lastName; //userInfo.family_name;
            bitscopicId.given_name = userFromDb.firstName; // userInfo.given_name;
            bitscopicId.email = userFromDb.email; // userInfo.email;
            bitscopicId.accessTokens = new List<AccessToken>();
            bitscopicId.accessTokens.Add(new AccessToken() { issuer = AccessTokenIssuer.VA_IAM, type = AccessTokenType.ACCESS_TOKEN, value = userInfo.accessToken.access_token });
            bitscopicId.accessTokens.Add(new AccessToken() { issuer = AccessTokenIssuer.VA_IAM, type = AccessTokenType.REFRESH_TOKEN, value = userInfo.accessToken.refresh_token });
            bitscopicId.aliases = new List<string>();
            foreach (Identity id in userFromDb.identities)
            {
                bitscopicId.aliases.Add(id.id);
            };

            String jwtForClient = new JWTUtils().getBitscopicIdentityJWTToken(key: authServiceClient.sharedSecret, clientId: authServiceClient.clientId, bitscopicIdentity: bitscopicId);

            if (String.Equals("PRAEDICARE", authServiceClient.clientId, StringComparison.CurrentCultureIgnoreCase) && 
                (MyConfigurationManager.getValue("PRAEDICARE_USE_OLD_JWT_FORMAT") == "true" || ConfigurationManager.AppSettings["PRAEDICARE_USE_OLD_JWT_FORMAT"] == "true"))
            {
                LogUtils.Log("Generating legacy JWT for PraediCare client...");
                jwtForClient = new JWTUtils().getJWTToken(key: authServiceClient.sharedSecret, clientId: authServiceClient.clientId, userInfo: userInfo, mappedUser: null);
            }

            // LogUtils.Log("JWT for client: " + jwtForClient);


            try
            {
                String clientRedirectUrl = retrievedClientState.redirect_uri + "?state=" + (String.IsNullOrEmpty(retrievedClientState.state) ? "none" : retrievedClientState.state) + "&jwt=" + jwtForClient;
                LogUtils.Log("Sending back to client app: " + clientRedirectUrl);
                Response.Redirect(clientRedirectUrl, true);
                return;
            }
            catch (Exception)
            {
                // swallow thread aborts
            }
        }

        string safeGetClaimValue(String key, IEnumerable<System.Security.Claims.Claim> claims)
        {
            if (claims != null && claims.Any(c => c.Type == key))
            {
                return claims.First(c => c.Type == key).Value;
            }
            return "";
        }

        private AuthServiceClient getAuthServiceClient(string clientId)
        {
            LogUtils.Log("Fetching auth service client " + clientId);
            if (String.IsNullOrEmpty(MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId)))
            {
                LogUtils.Log("Auth service client " + clientId + " not found!");
                return null;
            }
            LogUtils.Log("Client: " + SerializerUtils.serialize(new AuthServiceClient() { clientId = clientId, sharedSecret = MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId) }, false));
            return new AuthServiceClient() { clientId = clientId, sharedSecret = MyConfigurationManager.getValue("AuthServiceClientSharedSecret_" + clientId) };
        }

        private String retrieveState(string key)
        {
            if (String.IsNullOrEmpty(MyConfigurationManager.getValue(key)))
            {
                throw new ArgumentException("State not found for key: " + key);
            }
            return MyConfigurationManager.getValue(key);
        }

        //String getSTSToken(String iamSessionId, Dictionary<String, String> requestHeaders, Dictionary<String, HttpCookie> requestCookies)
        //{
        //    SecureTokenService sts = new SecureTokenService();
        //    return sts.getSTSToken(iamSessionId, requestHeaders, requestCookies);
        //}
    }

    public class IAMOAuth2AccessToken
    {
        public string access_token;
        public string refresh_token;
        public string scope;
        public string id_token;
        public string token_type;
        public string expires_in;

        public IAMOAuth2AccessToken() { }
    }

    public class IAMOAuth2UserInfo
    {
        public IAMOAuth2AccessToken accessToken;
        public string fediamMVIICN; // 1234567890V123456
        public string fediamCommonName; // e.g. vhapalmewtoj
        public string fediamSamAccountName; // e.g. vhapalmewtoj
        public string fediamsecid; // VA SEC ID
        public string fediamadUPN; //e.g. joel.mewton@va.gov
        public string email; 
        public string fediamAAL; // assurance level of identity - e.g. 3
        public string iat; 
        public string iss;
        public string at_hash;
        public string fediamadDomain; // e.g. vha21
        public string family_name;
        public string given_name;
        public string exp;
        public string aud;
        public string scope;
        private string _samlToken;

        public String getSAMLToken() { return _samlToken; }
        public void setSAMLToken(String saml) { _samlToken = saml; }
    }
}