﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using com.bitscopic.praediauth.domain;

namespace com.bitscopic.praediauth.utils
{
    [TestFixture]
    public class SerializerUtilsTest
    {

        [Test]
        public void testGetNameValuePairsJSON()
        {
            List<NameValuePair> userProps = new List<NameValuePair>();
            userProps.Add(new NameValuePair() { name = "firstName", value = "chris" });
            userProps.Add(new NameValuePair() { name = "role", value = "ACCOUNTUSER" });
            userProps.Add(new NameValuePair() { name = "email", value = "chris@bitscopic.com" });

            System.Console.WriteLine(SerializerUtils.serialize(userProps));
        }
    }
}
