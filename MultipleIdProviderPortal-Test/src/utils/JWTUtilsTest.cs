﻿using System;
using NUnit.Framework;
using com.bitscopic.praediauth.domain;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace com.bitscopic.praediauth.utils
{
    [TestFixture]
    public class JWTUtilsTest
    {
        [Test]
        public void testGetToken()
        {
            System.Console.WriteLine(new JWTUtils().getStrikedeckJWTToken("b!tsCOp!c", new com.bitscopic.praediauth. domain.BitscopicUser() { email = "joel.mewton@va.gov", firstName = "Joel", lastName = "Mewton", role = "ACCOUNTUSER" }));
        }

        [Test]
        public void testGetStrikedeckToken()
        {
            System.Console.WriteLine(new JWTUtils().getStrikedeckJWTToken("b!tsCOp!c", new com.bitscopic.praediauth.domain.BitscopicUser() { email = "joel.mewton@va.gov", firstName = "Joel", lastName = "Mewton", role = "ACCOUNTUSER" }));
            return;

            System.Console.WriteLine(
                "https://support.bitscopic.com/access/jwt?jwt="
                + new JWTUtils().getZendeskJWTToken("b!tsCOp!c",
                        new domain.BitscopicUser() { organization = "Bitscopic", email = "joel@bitscopic.com", firstName = "Joel", lastName = "Mewton" }));

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);

            //      List<BitscopicUser> users = getUsersFromFile();

            //    foreach (BitscopicUser bu in users)
            //    {
            ////        System.Console.WriteLine("Sending request to create " + bu.firstName + " " + bu.lastName);
            ////        String response = HttpUtils.Get(new Uri("https://bitscopic.zendesk.com/access/jwt?jwt=" + new JWTUtils().getZendeskJWTToken("zHCKvWicjgdfgDRYx5NFi9hkdwzUbv3BSML59s1oagszNgYC", bu)), "");
            //    }
            //    System.Console.WriteLine(response);
        }

        [Test]
        public void testGetZendeskToken()
        {
            System.Console.WriteLine(
                "https://bitscopic.zendesk.com/access/jwt?jwt="
                + new JWTUtils().getZendeskJWTToken("zHCKvWicjgdfgDRYx5NFi9hkdwzUbv3BSML59s1oagszNgYC",
                        new domain.BitscopicUser() { organization = "Bitscopic", email = "joel@bitscopic.com", firstName = "Joel", lastName = "Mewton" }));

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);

      //      List<BitscopicUser> users = getUsersFromFile();

        //    foreach (BitscopicUser bu in users)
        //    {
        ////        System.Console.WriteLine("Sending request to create " + bu.firstName + " " + bu.lastName);
        ////        String response = HttpUtils.Get(new Uri("https://bitscopic.zendesk.com/access/jwt?jwt=" + new JWTUtils().getZendeskJWTToken("zHCKvWicjgdfgDRYx5NFi9hkdwzUbv3BSML59s1oagszNgYC", bu)), "");
        //    }
        //    System.Console.WriteLine(response);
        }

        static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        List<BitscopicUser> getUsersFromFile()
        {
            String[] lines = System.IO.File.ReadAllLines(@"C:\users\joel\desktop\strikedeck_users.txt");
            List<BitscopicUser> result = new List<BitscopicUser>();
            foreach (String line in lines)
            {
                if (String.IsNullOrEmpty(line) || !line.Contains("\t"))
                {
                    continue;
                }

                String[] pieces = line.Split(new char[] { '\t' });
                if (pieces.Length != 3)
                {
                    continue;
                }

                result.Add(new BitscopicUser() { organization = "VA", email = pieces[2], lastName = pieces[1], firstName = pieces[0] });
            }

            return result;
        }
    }
}
