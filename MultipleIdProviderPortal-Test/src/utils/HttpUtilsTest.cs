﻿using System;
using System.Configuration;
using NUnit.Framework;

namespace com.bitscopic.idportal.utils
{
    [TestFixture]
    public class HttpUtilsTest
    {
        [Test]
        public void testGetReturnStringEmpty()
        {
            String rawUrl = "https://login.bitscopic.com/login.aspx";
            Assert.AreEqual(String.Empty, HttpUtils.getFormattedRedirectURLFromRequest(rawUrl));
        }

        [Test]
        public void testGetReturnStringZendesk()
        {
            ConfigurationManager.AppSettings.Set("JWTType", "Zendesk");
            String rawUrl = "https://login.bitscopic.com/login.aspx?return_to=https://bitscopic.zendesk.com/ticket/123";
            Assert.AreEqual("return_to=https://bitscopic.zendesk.com/ticket/123", HttpUtils.getFormattedRedirectURLFromRequest(rawUrl));
        }

        [Test]
        public void testGetReturnStringSD()
        {
            ConfigurationManager.AppSettings.Set("JWTType", "Strikedeck");
            String rawUrl = "https://login.bitscopic.com/login.aspx?returnTo=https://support.bitscopic.com/ticket/123";
            Assert.AreEqual("returnTo=https://support.bitscopic.com/ticket/123", HttpUtils.getFormattedRedirectURLFromRequest(rawUrl));
        }
    }
}
