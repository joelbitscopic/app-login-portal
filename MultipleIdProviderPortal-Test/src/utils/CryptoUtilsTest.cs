﻿using System;
using NUnit.Framework;

namespace com.bitscopic.praediauth.utils
{
    [TestFixture]
    public class CryptoUtilsTest
    {
        [Test]
        public void testHash()
        {
            String stringToHash = "BestestIT.1";
            String hash = CryptoUtils.hash(stringToHash);
            Assert.IsNotNull(hash);
            String hash2 = CryptoUtils.hash(stringToHash);
            Assert.AreEqual(hash, hash2);

            System.Console.WriteLine(hash);
        }
    }
}
