﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using com.bitscopic.praediauth.domain;

namespace com.bitscopic.praediauth.dao.sql
{
    [TestFixture]
    public class SqliteBitscopicUserDaoTest
    {
        SourceSystem _db = new SourceSystem() { type = SourceSystemType.SQLITE, connectionString = @"Data Source=C:\workspace\bitbucket\app-login-portal\MultipleIdProviderPortal\resources\db\db.sqlite" };
        
        [Test]
        public void testAddThenFetchUser()
        {
            BitscopicUser user = new BitscopicUser() { email = "joel@bitscopic.com", id = "1", firstName = "JOEL", lastName = "MEWTON" };
            user.identities = new List<Identity>() { new Identity() { provider = "IAM", id = "0003276914" } };
            IBitscopicUserDao dao = SqlDaoFactory.getBitscopicUserDao(SqlConnectionFactory.getConnectionBySource(_db));

            // user = dao.createBitscopicUser(user);

            //  Assert.IsFalse(String.IsNullOrEmpty(user.dbKey));
            //  Assert.IsFalse(String.IsNullOrEmpty(user.identities[0].dbKey));
            BitscopicUser fromDb = dao.getUser(user.id);

            Identity id = dao.addIdentity(fromDb.dbKey, new Identity() { id = "joel.mewton@va.gov", provider = "VA_EMAIL" });
            Assert.IsFalse(String.IsNullOrEmpty(id.dbKey));

            Assert.IsFalse(String.IsNullOrEmpty(fromDb.dbKey));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.email));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.firstName));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.lastName));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].dbKey));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].id));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].provider));

            fromDb = dao.getUserByIdentity(fromDb.identities[0].provider, fromDb.identities[0].id);
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.dbKey));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.email));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.firstName));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.lastName));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].dbKey));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].id));
            Assert.IsFalse(String.IsNullOrEmpty(fromDb.identities[0].provider));


        }


        //[Test]
        //public void testAddAllUsers()
        //{
        //    String praediUsersFile = System.IO.File.ReadAllText("Z:\\downloads\\praediusers.txt");
        //    String[] lines = praediUsersFile.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

        //    foreach (String line in lines)
        //    {
        //        String[] pieces = line.Split(new char[] { '|' });

        //        String firstName = pieces[0].Replace("\"", "");
        //        String lastName = pieces[1].Replace("\"", "");
        //        String email = pieces[2].Replace("\"", "");
        //        String products = pieces[3];

        //        BitscopicUser user = new BitscopicUser()
        //        {
        //            providersUserId = email,
        //            appUserId = email,
        //            firstName = firstName,
        //            lastName = lastName,
        //            email = email,
        //            products = SerializerUtils.deserialize<List<String>>(products),
        //            role = "ACCOUNTUSER"
        //        };

        //        String idProvider = "PIV";
        //        if (user.email.Contains("@va.gov"))
        //        {
        //            user.organization = "VA";

        //        }
        //        else
        //        {
        //            user.organization = "BITSCOPIC";
        //            idProvider = "GOOGLE";
        //            continue;
        //        }

        //        using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
        //        {
        //            cxn.Open();
        //            SqliteUserDao userDao = new SqliteUserDao(cxn);
        //            user = userDao.getUserByIdAndIdentityProvider(user.email, idProvider);
        //        }

        //        System.Console.WriteLine(new JWTUtils().getStrikedeckJWTToken(ConfigurationManager.AppSettings["StrikedeckSharedSecret"], user));

        //        //using (SQLiteConnection cxn = new SQLiteConnection(ConfigurationManager.AppSettings["SQLiteDbConnectionString"]))
        //        //{
        //        //    cxn.Open();
        //        //    SqliteUserDao userDao = new SqliteUserDao(cxn);
        //        //    Assert.IsNotNull(userDao.saveNewUser(user, idProvider));
        //        //}

        //    }
        //}
    }
}
